-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-06-2019 a las 00:17:18
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `appbd`
DROP DATABASE IF EXISTS appbd;

CREATE DATABASE appbd CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE appbd;
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo`
--

CREATE TABLE `catalogo` (
  `id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `detalle_id` int(11) NOT NULL,
  `estado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `catalogo`
--

INSERT INTO `catalogo` (`id`, `producto_id`, `detalle_id`, `estado`) VALUES
(1, 1, 1, 'disponible'),
(2, 2, 2, 'disponible'),
(3, 3, 3, 'disponible'),
(4, 4, 4, 'disponible'),
(5, 5, 5, 'disponible'),
(6, 6, 6, 'disponible'),
(7, 7, 7, 'disponible'),
(8, 8, 8, 'disponible'),
(9, 9, 9, 'disponible'),
(10, 10, 10, 'disponible'),
(11, 11, 11, 'disponible'),
(12, 13, 13, 'disponible'),
(13, 14, 14, 'disponible'),
(14, 15, 15, 'disponible'),
(15, 16, 16, 'disponible');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET latin1 NOT NULL,
  `apellido` varchar(50) CHARACTER SET latin1 NOT NULL,
  `ident` varchar(20) CHARACTER SET latin1 NOT NULL,
  `telefono` varchar(20) CHARACTER SET latin1 NOT NULL,
  `direccion` varchar(30) CHARACTER SET latin1 NOT NULL,
  `usuarios_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`, `apellido`, `ident`, `telefono`, `direccion`, `usuarios_id`) VALUES
(1, 'alejandro', 'vélez grajales', '1007233766', '2779854', 'calle 46', 1),
(2, 'pedro', 'pérez jimenez', '0292909309', '9083498', 'calle 43', 2),
(3, 'juan', 'rodriguez', '8309209380', '4356754', 'calle 23', 3),
(4, 'cata', 'garces', '9843972873', '7398723', 'calle 34', 4),
(5, 'jose', 'perez', '0248983937', '7948474', 'calle 34', 5),
(6, 'esteban', 'velez restrepo', '1008766544', '8776543', 'calle 23', 6),
(7, 'andres', 'perez', '1223544355', '1324354', 'calle 46 no 46-11', 7),
(8, 'luisa', 'perez', '9827398739', '9873973', 'calle 23 No 23-11', 8),
(9, 'isaac', 'perez', '9827398739', '2323323', 'isaac@gmail.com', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_insumos`
--

CREATE TABLE `compras_insumos` (
  `id` int(11) NOT NULL,
  `empleado_id` int(11) NOT NULL,
  `insumo_id` int(11) NOT NULL,
  `proveedor_ins_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precion` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deseados`
--

CREATE TABLE `deseados` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `catalogo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedido`
--

CREATE TABLE `detalle_pedido` (
  `id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `talla_nombre` varchar(4) NOT NULL,
  `cantidad` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_pedido`
--

INSERT INTO `detalle_pedido` (`id`, `pedido_id`, `producto_id`, `talla_nombre`, `cantidad`) VALUES
(1, 19, 12, 'S', 1),
(2, 19, 11, 'S', 1),
(3, 20, 6, 'S', 1),
(4, 20, 9, 'S', 1),
(5, 21, 1, 'S', 1),
(6, 22, 9, 'S', 1),
(7, 22, 10, 'S', 1),
(8, 22, 12, 'S', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `ident` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `direccion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `nombre`, `apellido`, `ident`, `telefono`, `direccion`) VALUES
(1, 'camila', 'perez', '1008223766', '2998765', 'calle 23'),
(2, 'juan', 'rodriguez', '1008223765', '2997634', 'calle 43'),
(3, 'Hamilton', 'Duncan', '16940401 5787', '16850929 4644', 'P.O. Box 578, 5537 Diam Street'),
(4, 'Evan', 'Kadeem', '16660928 9233', '16750110 4512', '913-3160 Lacus. St.'),
(5, 'Wyatt', 'Merrill', '16480628 5641', '16490928 8104', '329-7185 Fringilla Rd.'),
(6, 'Matthew', 'Edan', '16280913 4824', '16630804 9870', 'Ap #520-1296 Molestie Rd.'),
(7, 'Oren', 'Lester', '16080827 1175', '16051228 7970', 'Ap #375-1899 Eget Road'),
(8, 'Daquan', 'Rajah', '16571006 3016', '16411221 5100', '4171 Magna Ave'),
(9, 'Ray', 'Abraham', '16090506 2774', '16210509 2395', 'Ap #683-5372 Sit Street'),
(10, 'Peter', 'Gavin', '16990103 3010', '16430507 3704', 'Ap #700-5085 Id, Road');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `correo` tinytext NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `nit` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumos`
--

CREATE TABLE `insumos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `unidad_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `insumos`
--

INSERT INTO `insumos` (`id`, `nombre`, `cantidad`, `unidad_id`) VALUES
(2, 'tela para jean', 5, 1),
(3, 'pantalones', 3, 1),
(5, 'tela para camisa pol', 11, 1),
(6, 'tela para bermudas', 16, 1),
(7, 'tela para pantalonet', 32, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumos_proveedores`
--

CREATE TABLE `insumos_proveedores` (
  `id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `insumo_id` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `unidad_id` int(11) NOT NULL,
  `cantidad` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL,
  `prod_det_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`id`, `prod_det_id`, `cantidad`) VALUES
(1, 1, 10),
(2, 2, 12),
(3, 3, 12),
(4, 4, 12),
(5, 5, 12),
(6, 6, 12),
(7, 7, 12),
(8, 8, 12),
(9, 9, 12),
(10, 10, 12),
(11, 11, 12),
(12, 12, 12),
(13, 13, 12),
(14, 14, 4),
(15, 15, 10),
(16, 16, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id`, `cliente_id`, `estado`, `fecha`) VALUES
(19, 1, 'finalizado', '2019-06-07'),
(20, 3, 'finalizado', '2019-06-07'),
(21, 1, 'finalizado', '2019-06-11'),
(22, 1, 'finalizado', '2019-06-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `tipo_id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` text,
  `referencia` varchar(50) DEFAULT NULL,
  `precio` varchar(10) DEFAULT NULL,
  `imagenRuta` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `tipo_id`, `nombre`, `descripcion`, `referencia`, `precio`, `imagenRuta`) VALUES
(1, 1, 'camiseta', 'camiseta descripcion', '12345', '40000', '07-2836317-1.jpg'),
(2, 13, 'pantalon', 'pantalon descripcion', '54321', '30000', '07-1134988-1.jpg'),
(3, 6, 'buso', 'buso descripcion', '34567', '60000', '07-4135466-1.jpg'),
(4, 14, 'blusa', 'blusa descripcion', '54567', '40000', '07-3236191-1.jpg'),
(5, 5, 'sudadera', 'sudadera descripcion', '90984', '40000', '07-2335604-2.jpg'),
(6, 1, 'camiseta', 'camiseta descripcion', '98029', '40000', '07-5240820-1.jpg'),
(7, 15, 'pijama', 'pijama descripcion', '09830', '40000', '07-4036956-1.jpg'),
(8, 4, 'vestido', 'vestido descripcion', '87392', '50000', '07-1336953-1.jpg'),
(9, 6, 'buso de hombre', 'buso descripcion', '90840', '70000', '07-4336305-1.jpg'),
(10, 6, 'buso con figuras', 'buso descripcion', '08300', '90000', '07-5036310-1.jpg'),
(11, 8, 'chaqueta', 'chaqueta descripcion', '09380', '80000', '07-4736313-1.jpg'),
(12, 1, 'camiseta amarilla', 'camiseta descripcion', '09123', '50000', '07-1035536-1.jpg'),
(13, 1, 'camiseta azul oscura', 'camiseta descripcion', '98329', '80000', '07-5034208-1.jpg'),
(14, 14, 'blusa', 'blusa descripción', '32380', '120000', '11-0236969-1.jpg'),
(15, 1, 'camiseta', 'camiseta descripcion', '09145', '50000', '13-4536276-1.jpg'),
(16, 1, 'camiseta', 'camiseta descripcion', '09800', '70000', '13-1136273-1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_detalle`
--

CREATE TABLE `producto_detalle` (
  `id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `talla_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto_detalle`
--

INSERT INTO `producto_detalle` (`id`, `producto_id`, `talla_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 1),
(5, 5, 2),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1),
(14, 14, 2),
(15, 15, 2),
(16, 16, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `nombre_ger` tinytext NOT NULL,
  `correo` tinytext NOT NULL,
  `correo_ger` tinytext NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `telefono_ger` varchar(20) NOT NULL,
  `direccion` varchar(20) NOT NULL,
  `nit` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id`, `nombre`, `nombre_ger`, `correo`, `correo_ger`, `telefono`, `telefono_ger`, `direccion`, `nit`) VALUES
(1, 'coltejer', 'pedro', 'coltejer@gmail.com', 'pedro@gmail.com', '9873286', '8947876', 'calle 34', '1234567891'),
(3, 'telas', 'camilo', 'telas@gmail.com', 'camilo@gmail.com', '8917297', '9479826', 'calle 43', '1234567892'),
(4, 'textiles sas', 'jose', 'textiles@gmail.com', 'jose@gmail.com', '2356875', '8765432', 'calle 24', '6453324567'),
(5, 'expocorte', 'alejandro', 'expocorte@gmail.com', 'alejo11@gmail.com', '9874389', '8743987', 'calle 43', '7623786323');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`, `descripcion`) VALUES
(1, 'admin', 'Administrador del sistema'),
(2, 'empleado', 'Empleado de la empresa'),
(3, 'cliente', 'Usuario promedio de la app');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tallas`
--

CREATE TABLE `tallas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tallas`
--

INSERT INTO `tallas` (`id`, `nombre`) VALUES
(1, 'S'),
(2, 'M'),
(3, 'L'),
(4, 'Xl'),
(5, 'XXl'),
(6, 'XS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE `tipos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`) VALUES
(1, 'camiseta'),
(2, 'pantalon'),
(3, 'bufanda'),
(4, 'vestido'),
(5, 'sudadera'),
(6, 'buso'),
(8, 'chaqueta'),
(9, 'shorts'),
(10, 'camiseta polo'),
(11, 'jogger'),
(12, 'vestido largo'),
(13, 'pantaloneta'),
(14, 'blusa'),
(15, 'pijama');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidades`
--

CREATE TABLE `unidades` (
  `id` int(11) NOT NULL,
  `nombre_unidad` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidades`
--

INSERT INTO `unidades` (`id`, `nombre_unidad`) VALUES
(1, 'Metros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `correo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `rol_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `correo`, `contrasena`, `rol_id`) VALUES
(1, 'alejovg0911@gmail.com', 'alejo0911', 1),
(2, 'pedro@gmail.com', 'pedro0911', 3),
(3, 'juan@gmail.com', 'juan0720', 3),
(4, 'cata@gmail.com', 'cata0911', 3),
(5, 'jose@gmail.com', 'jose0911', 3),
(6, 'esteban@gmail.com', 'Esteban0720*', 3),
(7, 'andres@gmail.com', 'andres123', 3),
(8, 'luisa@gmail.com', 'luisa0911', 3),
(9, 'hola@gmail.com', 'alejo0911', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `catalogo`
--
ALTER TABLE `catalogo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventario_id` (`producto_id`),
  ADD KEY `detalle_id` (`detalle_id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuarios_correo` (`usuarios_id`);

--
-- Indices de la tabla `compras_insumos`
--
ALTER TABLE `compras_insumos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empleado_id` (`empleado_id`),
  ADD KEY `insumo_id` (`insumo_id`),
  ADD KEY `proveedor_ins_id` (`proveedor_ins_id`);

--
-- Indices de la tabla `deseados`
--
ALTER TABLE `deseados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalogo_id` (`catalogo_id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `producto_id` (`producto_id`),
  ADD KEY `pedido_id` (`pedido_id`),
  ADD KEY `talla_id` (`talla_nombre`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `insumos`
--
ALTER TABLE `insumos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unidad_id` (`unidad_id`);

--
-- Indices de la tabla `insumos_proveedores`
--
ALTER TABLE `insumos_proveedores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proveedor_id` (`proveedor_id`),
  ADD KEY `insumo_id` (`insumo_id`),
  ADD KEY `unidad_id` (`unidad_id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prod_det_id` (`prod_det_id`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_id` (`tipo_id`);

--
-- Indices de la tabla `producto_detalle`
--
ALTER TABLE `producto_detalle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `producto_id` (`producto_id`),
  ADD KEY `talla_id` (`talla_id`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tallas`
--
ALTER TABLE `tallas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `unidades`
--
ALTER TABLE `unidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`,`correo`),
  ADD KEY `usuarios_ibfk_1` (`rol_id`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedido_id` (`pedido_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `catalogo`
--
ALTER TABLE `catalogo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `compras_insumos`
--
ALTER TABLE `compras_insumos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `deseados`
--
ALTER TABLE `deseados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `insumos`
--
ALTER TABLE `insumos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `insumos_proveedores`
--
ALTER TABLE `insumos_proveedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `producto_detalle`
--
ALTER TABLE `producto_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tallas`
--
ALTER TABLE `tallas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `unidades`
--
ALTER TABLE `unidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `catalogo`
--
ALTER TABLE `catalogo`
  ADD CONSTRAINT `catalogo_ibfk_1` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `catalogo_ibfk_2` FOREIGN KEY (`detalle_id`) REFERENCES `producto_detalle` (`id`);

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `compras_insumos`
--
ALTER TABLE `compras_insumos`
  ADD CONSTRAINT `compras_insumos_ibfk_1` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`),
  ADD CONSTRAINT `compras_insumos_ibfk_2` FOREIGN KEY (`insumo_id`) REFERENCES `insumos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `compras_insumos_ibfk_3` FOREIGN KEY (`proveedor_ins_id`) REFERENCES `insumos_proveedores` (`id`);

--
-- Filtros para la tabla `deseados`
--
ALTER TABLE `deseados`
  ADD CONSTRAINT `deseados_ibfk_1` FOREIGN KEY (`catalogo_id`) REFERENCES `catalogo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `deseados_ibfk_2` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD CONSTRAINT `detalle_pedido_ibfk_1` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `detalle_pedido_ibfk_2` FOREIGN KEY (`pedido_id`) REFERENCES `pedidos` (`id`);

--
-- Filtros para la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD CONSTRAINT `empresas_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `insumos`
--
ALTER TABLE `insumos`
  ADD CONSTRAINT `insumos_ibfk_1` FOREIGN KEY (`unidad_id`) REFERENCES `unidades` (`id`);

--
-- Filtros para la tabla `insumos_proveedores`
--
ALTER TABLE `insumos_proveedores`
  ADD CONSTRAINT `insumos_proveedores_ibfk_1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedores` (`id`),
  ADD CONSTRAINT `insumos_proveedores_ibfk_2` FOREIGN KEY (`insumo_id`) REFERENCES `insumos` (`id`),
  ADD CONSTRAINT `insumos_proveedores_ibfk_3` FOREIGN KEY (`unidad_id`) REFERENCES `unidades` (`id`);

--
-- Filtros para la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `inventario_ibfk_1` FOREIGN KEY (`prod_det_id`) REFERENCES `producto_detalle` (`id`);

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedidos_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`);

--
-- Filtros para la tabla `producto_detalle`
--
ALTER TABLE `producto_detalle`
  ADD CONSTRAINT `producto_detalle_ibfk_1` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `producto_detalle_ibfk_3` FOREIGN KEY (`talla_id`) REFERENCES `tallas` (`id`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_ibfk_1` FOREIGN KEY (`pedido_id`) REFERENCES `pedidos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
