$(document).ready(function() {
    $("#registClientes").click(function() {
        $("#card-clientes").css({
            "right": "0",
            "transition": "0.6s"
        });
        $("#close").click(function() {
            $("#card-clientes").css({
                "right": "-100" + "%",
                "transition": "0.6s"
            });
        });
    });
});
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $(".icon-up").slideDown();
    } else {
        $(".icon-up").slideUp();
    }
});

$(".icon-up").click(function() {
    $("html,body").animate({ scrollTop: 0 }, 600);
});

let salir = document.getElementById('salir')
salir.addEventListener('click',()=>{
    localStorage.removeItem('usuario')
})

let content_data = document.getElementById("content_data")
const fecthAll = () => {
    let content_data = document.getElementById("content_data")
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/client/read.client.php",true)
    xhr.addEventListener("load",()=>{
        let clients = JSON.parse(xhr.responseText)
        let templateClients =""
        clients.forEach(client => {
            templateClients+=`
            <tr>
                <td>${client.nombre}</td>
                <td>${client.apellido}</td>
                <td>${client.ident}</td>
                <td>${client.telefono}</td>
                <td>${client.correo}</td>
                <td>${client.direccion}</td>
                <td>
                    <a href=""> <button class="btn btn-danger btn-sm">Elminar</button> </a>
                </td>
            </tr>
            `
        })
        content_data.innerHTML = templateClients
    })
    xhr.send()
}
fecthAll()


// -----------------------Buscar----------------------------

let searchInput = document.getElementById("searchInput")
searchInput.addEventListener("keyup",()=>{

  const xhr = new XMLHttpRequest
  let searchValue = searchInput.value
  xhr.open("POST","../../../model/client/search.client.php",true)
  xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
  xhr.addEventListener("load",()=>{
    let clients = JSON.parse(xhr.responseText);
    let templateClients = ''
    if (clients.length == 0) {
        templateClients+=`
        <tr>
            <td colspan="6" align="center">No se encontraron resultados</td>
        </tr>
         ` 
        content_data.innerHTML = templateEmployees    
    }else{
        clients.forEach(client => {
            templateClients+=`
            <tr>
                <td>${client.nombre}</td>
                <td>${client.apellido}</td>
                <td>${client.ident}</td>
                <td>${client.telefono}</td>
                <td>${client.correo}</td>
                <td>${client.direccion}</td>
                <td>
                    <a href=""> <button class="btn btn-danger btn-sm">Elminar</button> </a>
                </td>
            </tr>
            `          
            content_data.innerHTML = templateClients    
        });
    }
  })

  xhr.send(`search=${searchValue}`)
})