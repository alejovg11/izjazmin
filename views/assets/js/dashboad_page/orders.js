$(document).ready(function() {
    $("#registPedidos").click(function() {
        $("#card-pedidos").css({
            "right": "0",
            "transition": "0.6s"
        });
        $("#close").click(function() {
            $("#card-pedidos").css({
                "right": "-100" + "%",
                "transition": "0.6s"
            });
        });
    });
});
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $(".icon-up").slideDown();
    } else {
        $(".icon-up").slideUp();
    }
});

$(".icon-up").click(function() {
    $("html,body").animate({ scrollTop: 0 }, 600);
});

let salir = document.getElementById('salir')
salir.addEventListener('click',()=>{
    localStorage.removeItem('usuario')
})

// -----------------------listar----------------------------

const fecthAll = () => {
    let content_data = document.getElementById("content_data")
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/orders/read.orders.php",true)
    xhr.addEventListener("load",()=>{
        let orders = JSON.parse(xhr.responseText)
        console.log(orders);
        let templateOrders =""
        orders.forEach(order => {
            templateOrders+=`
                <tr data-id="${order.id}">
                    <td title="numeroPedido">${order.id}</td>
                    <td title="cliente">
                       <a href="../../modules/dashboard_page/orders_details.php?id=${order.id}">${order.nombre}</a>
                    </td>
                    <td title="estado">${order.estado}</td>
                    <td title="fecha">${order.fecha}</td>
                    <td>
                        <button class="btn btn-success btn-sm" id="${order.id}" type="submit">${order.estado === "finalizado" ? '<i class="fas fa-check"></i>' : "Despachar"}</button>
                    </td>
                </tr>
            `
        })
        content_data.innerHTML=templateOrders
    })
    xhr.send()
}
fecthAll()

content_data.addEventListener("click",(e)=>{
    const xhr = new XMLHttpRequest
    if (e.target.tagName === "BUTTON" && e.target.textContent === "Despachar") {
        xhr.open("POST","../../../model/orders/update.orders.php",true)
        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
        xhr.addEventListener("load",() => {
            e.target.innerHTML = `<i class="fas fa-spinner fa-spin"></i>`
            setTimeout(() => {
                fecthAll()
            }, 1500);
        })
        xhr.send(`id=${e.target.id}`)
    } 
    
})
