$(document).ready(function() {
    $("#registUsuarios").click(function() {
        $("#card-usuarios").css({
            "right": "0",
            "transition": "0.6s"
        });
        $("#close").click(function() {
            $("#card-usuarios").css({
                "right": "-100" + "%",
                "transition": "0.6s"
            });
        });
    });
});
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $(".icon-up").slideDown();
    } else {
        $(".icon-up").slideUp();
    }
});

$(".icon-up").click(function() {
    $("html,body").animate({ scrollTop: 0 }, 600);
});

let salir = document.getElementById('salir')
salir.addEventListener('click',()=>{
    localStorage.removeItem('usuario')
})

// -----------------------listar----------------------------

let content_data = document.getElementById("content_data")
const fecthAll = () => {
    let content_data = document.getElementById("content_data")
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/users/read.users.php",true)
    xhr.addEventListener("load",()=>{
        let users = JSON.parse(xhr.responseText)
        let templateUsers =""
        users.forEach(user => {
            templateUsers+=`
                <tr data-id="${user.id}">
                    <td title="correo">${user.correo}</td>
                    <td title="contraseña">${user.contrasena}</td>
                    <td title="rol">${user.nombre}</td>
                    <td>
                        <button class="btn btn-danger btn-sm" id="${user.id}" type="submit">Eliminar</button>
                    </td>
                </tr>
            `
        })
        content_data.innerHTML=templateUsers
    })
    xhr.send()
}
fecthAll()

// -----------------------Eliminar----------------------------

content_data.addEventListener("click",(e)=>{
    const xhr = new XMLHttpRequest
    if (e.target.tagName === "BUTTON" && e.target.textContent === "Eliminar") {
        $.confirm({
            title: 'Eliminar usuario',
            autoClose: 'Cancelar|8000',
            buttons: {
                deleteUser: {
                    text: 'Sí, eliminar',
                    action: function () {
                        toastr.success("Se eliminó correctamente.")
                        xhr.open("POST","../../../model/users/delete.users.php",true)
                        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
                        xhr.addEventListener("load",() => {
                            fecthAll()
                        })
                        xhr.send(`id=${e.target.id}`)
                    }
                },
                Cancelar:{
                    title: 'nose',
                    cancelAction: function () {
                        $.alert('Cancelar');
                    }
                }
            }
        });
    }
})


// -----------------------Buscar----------------------------

let searchInput = document.getElementById("searchInput")
searchInput.addEventListener("keyup",()=>{

  const xhr = new XMLHttpRequest
  let searchValue = searchInput.value
  xhr.open("POST","../../../model/users/search.users.php",true)
  xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
  xhr.addEventListener("load",()=>{
    let users = JSON.parse(xhr.responseText);
    let templateUsers = ''
    if (users.length == 0) {
        templateUsers+=`
        <tr>
            <td colspan="6" align="center">No se encontraron resultados</td>
        </tr>
         ` 
        content_data.innerHTML = templateUsers    
    }else{
        users.forEach(user => {
            templateUsers+=`
            <tr data-id="${user.id}">
                <td title="correo">${user.correo}</td>
                <td title="contraseña">${user.contrasena}</td>
                <td title="rol">${user.nombre}</td>
                <td>
                    <button class="btn btn-danger btn-sm" id="${user.id}" type="submit">Eliminar</button>
                </td>
            </tr>
            `          
            content_data.innerHTML = templateUsers    
        });
    }
  })

  xhr.send(`search=${searchValue}`)
})


