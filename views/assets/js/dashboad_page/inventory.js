$(document).ready(function () {
    $("#añadirProducto").click(function () {
        $("#card-añadir-producto").css({
            "right": "0",
            "transition": "0.6s"
        });
    });
    $(".close").click(function () {
        $("#card-añadir-producto").css({
            "right": "-100" + "%",
            "transition": "0.6s"
        });
    });
    $("#añadirTipo").click(function () {
        $("#card-añadir-producto-tipo").css({
            "right": "0",
            "transition": "0.6s"
        });
    });
    $(".close").click(function () {
        $("#card-añadir-producto-tipo").css({
            "right": "-100" + "%",
            "transition": "0.6s"
        });
    });
});

$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $(".icon-up").slideDown();
    } else {
        $(".icon-up").slideUp();
    }
});

$(".icon-up").click(function () {
    $("html,body").animate({scrollTop: 0}, 600);
});

// -----------------------listar----------------------------

let content_data = document.getElementById("content_data")
const fecthAll = () => {
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/inventory/read.inventory.php",true)
    xhr.addEventListener("load",()=>{
        let inventories = JSON.parse(xhr.responseText)
        let templateInventories =""
        inventories.forEach(inventory => {
            templateInventories+=`
                <tr data-id="${inventory.id}">
                    <td title="nombre">${inventory.nombre}</td>
                    <td title="cantidad">${inventory.cantidad}</td>
                    <td title="referencia">${inventory.referencia}</td>
                    <td>
                        <button class="btn btn-danger btn-sm" id="${inventory.id}" type="submit">Eliminar</button>
                    </td>
                </tr>
            `
        })
        content_data.innerHTML=templateInventories
    })
    xhr.send()
}
fecthAll()

// -----------------------listar tipos de prenda----------------------------

let content_data_types = document.getElementById("content_data_types")
const fecthAllTypes = () => {
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/inventory/read.inventory.tipos.php",true)
    xhr.addEventListener("load", () => {
        let tipos = JSON.parse(xhr.responseText)
        let templateTipos =""
        tipos.forEach(tipo => {
            templateTipos+=`
                <option value="${tipo.id}">${tipo.nombre}</option>  
            `
        })
        content_data_types.innerHTML=templateTipos
    })
    xhr.send()
}
fecthAllTypes()


let content_data_tallas = document.getElementById("content_data_tallas")
const fecthAllTallas = () => {
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/inventory/read.inventory.talla.php",true)
    xhr.addEventListener("load",()=>{
        let tallas = JSON.parse(xhr.responseText)
        let templateTallas =""
        tallas.forEach(talla => {
            templateTallas+=`
                <option value="${talla.id}">${talla.nombre}</option>  
            `
        })
        content_data_tallas.innerHTML=templateTallas
    })
    xhr.send()
}
fecthAllTallas()

let close = document.getElementById("close")
let form_tipos = document.getElementById("form_tipos")
form_tipos.addEventListener("submit",(e) => {
    e.preventDefault()
    const xhr = new XMLHttpRequest
    const formData = new FormData(form_tipos)
    xhr.open("POST","../../../model/inventory/insert.inventory.tipos.php",true)
    xhr.addEventListener("load",() => {
        fecthAll()
        fecthAllTypes()
        let r = xhr.responseText
        if(r == 2){
            toastr.info("Este tipo de prenda ya existe, prueba con otro");
        }
        else{
            form_tipos.reset();
            close.click()
            toastr.success("Se registró correctamente.")
        }
    })
    xhr.send(formData)
})

let salir = document.getElementById('salir')
salir.addEventListener('click',()=>{
    localStorage.removeItem('usuario')
})

// -----------------------Insert de productos----------------------------
let icon_spinner= document.getElementById('icon_spinner')
icon_spinner.style.display="none"

let form_producto = document.getElementById("form_producto")


form_producto.addEventListener("submit",(e) => {
    e.preventDefault();
    const formData = new FormData(form_producto)
    $.ajax({
		url: "../../../model/inventory/insert.inventory.producto.php",
		type: "POST",
		dataType: "HTML",
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	}).done(function(echo){
        fecthAllReferences()
        setTimeout(() => {
            fecthAll()
            icon_spinner.style.display="none"
        }, 2000);
        close.click()
        form_producto.reset()
        let inputclases = form_producto.querySelectorAll("input");
        inputclases.forEach(element => {
            element.classList.remove("is-valid")
        });
        toastr.success("Se registró correctamente.")
        icon_spinner.style.display=""
	});
})


let searchInput = document.getElementById("searchInput")
searchInput.addEventListener("keyup",()=>{

  const xhr = new XMLHttpRequest
  let searchValue = searchInput.value
  xhr.open("POST","../../../model/inventory/search.inventory.php",true)
  xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
  xhr.addEventListener("load",()=>{
    let inventories = JSON.parse(xhr.responseText);
    let templateInventories = ''
    if (inventories.length == 0) {
        templateInventories+=`
        <tr>
            <td colspan="6" align="center">No se encontraron resultados</td>
        </tr>
         ` 
        content_data.innerHTML = templateInventories    
    }else{
        inventories.forEach(inventory => {
            templateInventories+=`
            <tr data-id="${inventory.id}">
                <td title="nombre">${inventory.nombre}</td>
                <td title="cantidad">${inventory.cantidad}</td>
                <td title="referencia">${inventory.referencia}</td>
                <td>
                    <button class="btn btn-danger btn-sm" id="${inventory.id}" type="submit">Eliminar</button>
                </td>
            </tr>
            `          
            content_data.innerHTML = templateInventories    
        });
    }
  })

  xhr.send(`search=${searchValue}`)
})


// -----------------------Eliminar----------------------------

content_data.addEventListener("click",(e)=>{
    const xhr = new XMLHttpRequest
    if (e.target.tagName === "BUTTON" && e.target.textContent === "Eliminar") {
        $.confirm({
            title: 'Eliminar del inventario',
            autoClose: 'Cancelar|8000',
            buttons: {
                deleteUser: {
                    text: 'Sí, eliminar',
                    action: function () {
                        toastr.success("Se eliminó correctamente.")
                        xhr.open("POST","../../../model/inventory/delete.inventory.php",true)
                        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
                        xhr.addEventListener("load",() => {
                            fecthAll()
                        })
                        xhr.send(`id=${e.target.id}`)
                    }
                },
                Cancelar:{
                    title: 'nose',
                    cancelAction: function () {
                        $.alert('Cancelar');
                    }
                }
            }
        });
    }
})

// -----------------------Validate----------------------------

const inputs = Array.from(document.querySelectorAll("[required]"))

inputs.forEach(input => {
    let div = `<div class="invalid-feedback" id="${input.name}"></div>`
    input.parentElement.insertAdjacentHTML("beforeend",div)
        input.addEventListener("keyup",e=>{
            if (input.pattern) {
              let regex = new RegExp(input.pattern)
              if (!regex.exec(input.value)) {
                  document.querySelector(`#${input.name}`).innerHTML = `Introduce ${input.title}.`
                    input.classList.add("is-invalid")
                  
                }else{
                    input.classList.remove("is-invalid")
                    input.classList.add("is-valid")
                }   
            }
        })
});

// -----------------------Editar----------------------------
content_data.addEventListener("click",(e)=>{
    const xhr = new XMLHttpRequest
        if (e.target.tagName === "TD") {
            e.target.setAttribute("contenteditable", true)
            e.target.focus()
            e.target.setAttribute("spellcheck", false)
            
            e.target.addEventListener("keyup", e=> {
                let valorId = e.target.parentElement.dataset.id
                let nameUpdate = e.target.textContent
                let title = e.target.title

                if (e.keyCode === 13) {
                    save(nameUpdate,valorId,title)
                    e.target.blur()
                }    
            })
            const save = (valor,valorId,title) => {
                xhr.open("POST","../../../model/inventory/update.inventory.php",true)
                xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
                xhr.addEventListener("load",() => {
                    let r = xhr.responseText
                    fecthAll()                 
                    toastr.success("Se actualizó correctamente.")
                    
                })
                xhr.send(`valor=${valor}&id=${valorId}&title=${title}`)
            }
        }
})

    
// -----------------------Autocompletado----------------------------

const fecthAllReferences = () => {
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/inventory/read.inventory.references.php",true)
    xhr.addEventListener("load",()=>{
        let references = JSON.parse(xhr.responseText)
        let nameReferences = references.map(r => r.referencia)
        function autocompletar(array){
            let input = document.getElementById("input_referencia")
            let indexFocus = -1;
            input.addEventListener("input", (e)=>{
                const tipoDeMascota = e.target.value
                if (!tipoDeMascota) return false
                cerrarLista()
        
                // crear lista de sugerencias
                const divlist = document.createElement("div")
                divlist.setAttribute("id", `${e.target.id}-lista-autocompletar`)
                divlist.setAttribute("class", "lista-autocompletar-items")
                e.target.parentElement.insertAdjacentElement("beforeend", divlist)
        
                // validar el arreglo vs input
                if (array.length == 0) return false;
                array.forEach(mascota => {
                    if (mascota.substr(0, tipoDeMascota.length) == tipoDeMascota) {
                        const elementoLista = document.createElement("div")
                        elementoLista.innerHTML = `<strong>${mascota.substr(0, tipoDeMascota.length)}</strong>${mascota.substr(tipoDeMascota.length)}`
                        elementoLista.addEventListener("click",(e)=>{
                            input.value = e.target.innerText
                            cerrarLista()
                            return false;
                        })
                        divlist.appendChild(elementoLista)
                    }
                });
            })
            input.addEventListener("keydown", (e)=>{
                const divlist = document.getElementById(`${e.target.id}-lista-autocompletar`)
                let items
                if (divlist) {
                    items = divlist.querySelectorAll("div")
                    switch (e.keyCode) {
                        case 40: //tecla abajo
                            indexFocus++
                            if (indexFocus>items.length-1) indexFocus = items.length -1
                            break;
                        case 38: //tecla arriba
                            indexFocus--
                            if (indexFocus< 0) indexFocus = 0
                            break;
                        case 13: //tecla enter
                            e.preventDefault()
                            items[indexFocus].click()
                            indexFocus=-1
                            break;
                    
                        default:
                            break;
                    }
                    seleccionar(items, indexFocus);
                    return false
                }
            })
            document.addEventListener("click",()=>{
                cerrarLista()
            })
        }
        function seleccionar(items, indexFocus) {
            if (!items || indexFocus == -1) return false
            items.forEach(e => {
                e.classList.remove("autocompletar-active")
                items[indexFocus].classList.add("autocompletar-active")
            });
        }
        function cerrarLista(){
            const items = document.querySelectorAll(".lista-autocompletar-items");
            items.forEach(item=>{
                item.parentNode.removeChild(item)
            })
            indexFocus = -1
            
        }
        autocompletar(nameReferences)
    })
    xhr.send()
}
fecthAllReferences()
