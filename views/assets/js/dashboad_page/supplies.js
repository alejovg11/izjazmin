$(document).ready(function () {
    $("#registInsumos").click(function () {
        $("#card-insumos").css({
            "right": "0",
            "transition": "0.6s"
        });
    });
    $("#close").click(function () {
        $("#card-insumos").css({
            "right": "-100" + "%",
            "transition": "0.6s"
        });
    });

    $("#añadirInsumos").click(function () {
        $("#card-insumos-añadir").css({
            "right": "0",
            "transition": "0.6s"
        });
    });
    $("#close-añadir").click(function () {
        $("#card-insumos-añadir").css({
            "right": "-100" + "%",
            "transition": "0.6s"
        });
    });

});

$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $(".icon-up").slideDown();
    } else {
        $(".icon-up").slideUp();
    }
});

$(".icon-up").click(function () {
    $("html,body").animate({scrollTop: 0}, 600);
});

let salir = document.getElementById('salir')
salir.addEventListener('click',()=>{
    localStorage.removeItem('usuario')
})

// -----------------------listar----------------------------

let content_data = document.getElementById("content_data")
const fecthAll = () => {
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/supplies/read.supplies.php",true)
    xhr.addEventListener("load",()=>{
        let supplies = JSON.parse(xhr.responseText)
        let templateSupplies =""
        supplies.forEach(supplie => {
            templateSupplies+=`
                <tr data-id="${supplie.id}">
                    <td title="nombre">${supplie.nombre}</td>
                    <td title="cantidad">${supplie.cantidad}</td>
                    <td title="nombre_unidad">${supplie.nombre_unidad}</td>
                    <td>
                        <button class="btn btn-danger btn-sm" id="${supplie.id}" type="submit">Eliminar</button>
                    </td>
                </tr>
            `
        })
        content_data.innerHTML=templateSupplies
    })
    xhr.send()
}
fecthAll()

// -----------------------listar pedidos de insumos----------------------------

let content_data2 = document.getElementById("content_data2")
const fecthAll2 = () => {
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/supplies/read.orders.php",true)
    xhr.addEventListener("load",()=>{
        let suppliesOrders = JSON.parse(xhr.responseText)
        let templateSuppliesOrders =""
        suppliesOrders.forEach(suppliesOrder => {
            templateSuppliesOrders+=`
                <tr data-id="${suppliesOrder.id}">
                    <td title="nombre">${suppliesOrder.nombre}</td>
                    <td title="nombre_proveedor">${suppliesOrder.nombre_proveedor}</td>
                    <td title="cantidad">${suppliesOrder.cantidad}</td>
                    <td title="nombre_unidad">${suppliesOrder.nombre_unidad}</td>
                    <td title="precio">${suppliesOrder.precio}</td>
                    <td>
                        <button class="btn btn-danger btn-sm" id="${suppliesOrder.id}" type="submit">Eliminar</button>
                    </td>
                </tr>
            `
        })
        content_data2.innerHTML=templateSuppliesOrders
    })
    xhr.send()
}
fecthAll2()

// -----------------------Insertar----------------------------
let icon_spinner= document.getElementById('icon_spinner')
icon_spinner.style.display="none"
let form_supplies = document.getElementById("form_supplies")
let close = document.getElementById("close-añadir")
form_supplies.addEventListener("submit",(e) => {
    e.preventDefault()
    const xhr = new XMLHttpRequest
    const formData = new FormData(form_supplies)
    xhr.open("POST","../../../model/supplies/insert.supplies.php",true)
    xhr.addEventListener("load",() => {
        icon_spinner.style.display=""
        setTimeout(() => {
            fecthAll()
            icon_spinner.style.display="none"
        }, 2000);
        close.click()
        form_supplies.reset()
        toastr.success("Se registró correctamente.")
    })
    xhr.send(formData)
})

// -----------------------Insertar pedidos de insumos----------------------------

let form_orders = document.getElementById("form_orders")
let close_orders = document.getElementById("close")
form_orders.addEventListener("submit",(e) => {
    e.preventDefault()
    const xhr = new XMLHttpRequest
    const formData = new FormData(form_orders)
    xhr.open("POST","../../../model/supplies/insert.orders.php",true)
    xhr.addEventListener("load",() => {
        fecthAll2()
        close_orders.click()
        form_orders.reset()
        toastr.success("Se registró correctamente.")
    })
    xhr.send(formData)
})

// -----------------------Eliminar----------------------------

content_data.addEventListener("click",(e)=>{
    const xhr = new XMLHttpRequest
    if (e.target.tagName === "BUTTON" && e.target.textContent === "Eliminar") {
        $.confirm({
            title: 'Eliminar Empleado',
            autoClose: 'Cancelar|8000',
            buttons: {
                deleteUser: {
                    text: 'Sí, eliminar',
                    action: function () {
                        toastr.success("Se eliminó correctamente.")
                        xhr.open("POST","../../../model/supplies/delete.supplies.php",true)
                        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
                        xhr.addEventListener("load",() => {
                            fecthAll()
                        })
                        xhr.send(`id=${e.target.id}`)
                    }
                },
                Cancelar:{
                    title: 'nose',
                    cancelAction: function () {
                        $.alert('Cancelar');
                    }
                }
            }
        });
    }
})

// -----------------------Editar----------------------------

content_data.addEventListener("click",(e)=>{
    const xhr = new XMLHttpRequest
        if (e.target.tagName === "TD") {
            e.target.setAttribute("contenteditable", true)
            e.target.focus()
            e.target.setAttribute("spellcheck", false)
            
            e.target.addEventListener("keyup", e=> {
                let valorId = e.target.parentElement.dataset.id
                let nameUpdate = e.target.textContent
                let title = e.target.title

                if (e.keyCode === 13) {
                    save(nameUpdate,valorId,title)
                    e.target.blur()
                    toastr.success("Se actualizó correctamente.")
                }    
            })
            const save = (valor,valorId,title) => {
                xhr.open("POST","../../../model/supplies/update.supplies.php",true)
                xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
                xhr.addEventListener("load",() => {
                })
                xhr.send(`valor=${valor}&id=${valorId}&title=${title}`)
            }
        }
})

// validate

const inputs = Array.from(document.querySelectorAll("[required]"))

inputs.forEach(input => {
    let div =   `<div class="invalid-feedback" id="${input.name}"></div>`
    input.parentElement.insertAdjacentHTML("beforeend",div)
        input.addEventListener("keyup",e=>{
            if (input.pattern) {
              let regex = new RegExp(input.pattern)
              if (!regex.exec(input.value)) {
                  document.querySelector(`#${input.name}`).innerHTML = `Introduce ${input.title}.`
                    input.classList.add("is-invalid")
                  
                }else{
                    input.classList.remove("is-invalid")
                    input.classList.add("is-valid")
                }   
            }
        })
});