$(document).ready(function() {
    $("#registCatalogo").click(function() {        
        $("#card-catalogo").css({
            "right": "0",
            "transition": "0.6s"
        });
        $("#close").click(function() {
            $("#card-catalogo").css({
                "right": "-100" + "%",
                "transition": "0.6s"
            });
        });
    });
});
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $(".icon-up").slideDown();
    } else {
        $(".icon-up").slideUp();
    }
});

$(".icon-up").click(function() {
    $("html,body").animate({ scrollTop: 0 }, 600);
});

let salir = document.getElementById('salir')
salir.addEventListener('click',()=>{
    localStorage.removeItem('usuario')
})

// -----------------------listar----------------------------

let content_data = document.getElementById("content_data")
const fecthAll = () => {
    let content_data = document.getElementById("content_data")
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/catalog/read.catalog.php",true)
    xhr.addEventListener("load",()=>{
        let catalogs = JSON.parse(xhr.responseText)
        let templateCatalogs =""
        catalogs.forEach(catalog => {
            templateCatalogs+=`
                <tr data-id="${catalog.id}">
                    <td title="nombre">${catalog.nombre}</td>
                    <td title="descripcion">${catalog.descripcion}</td>
                    <td title="referencia">${catalog.referencia}</td>
                    <td title="precio">${catalog.precio}</td>
                    <td title="estado">${catalog.estado}</td>
                    <td title="imagen">
                        <img class="img-fluid image" src="../../assets/images/shop_page/clothes/${catalog.imagenRuta}" alt="Card image cap ">
                    </td>
                    <td>
                        <button class="btn btn-danger btn-sm" id="${catalog.id}" type="submit">Eliminar</button>
                    </td>
                </tr>
            `
        })
        content_data.innerHTML=templateCatalogs
    })
    xhr.send()
}
fecthAll()


// -----------------------Insertar----------------------------
let icon_spinner= document.getElementById('icon_spinner')
icon_spinner.style.display="none"


let form_catalog = document.getElementById("form_catalog")
let close = document.getElementById("close")
form_catalog.addEventListener("submit",(e) => {
    e.preventDefault()
    const xhr = new XMLHttpRequest
    const formData = new FormData(form_catalog)
    xhr.open("POST","../../../model/catalog/insert.catalog.php",true)
    xhr.addEventListener("load",() => {
        icon_spinner.style.display=""
        setTimeout(() => {
            fecthAll()
            icon_spinner.style.display="none"
        }, 2000);
        close.click()
        form_catalog.reset()
        toastr.success("Se registró correctamente.")
    })
    xhr.send(formData)
})

// validate

const inputs = Array.from(document.querySelectorAll("[required]"))

inputs.forEach(input => {
    let div =   `<div class="invalid-feedback" id="${input.name}"></div>`
    input.parentElement.insertAdjacentHTML("beforeend",div)
        input.addEventListener("keyup",e=>{
            if (input.pattern) {
              let regex = new RegExp(input.pattern)
              if (!regex.exec(input.value)) {
                  document.querySelector(`#${input.name}`).innerHTML = `Introduce ${input.title}.`
                    input.classList.add("is-invalid")
                  
                }else{
                    input.classList.remove("is-invalid")
                    input.classList.add("is-valid")
                }   
            }
        })
});


// -----------------------Eliminar----------------------------

content_data.addEventListener("click",(e)=>{
    const xhr = new XMLHttpRequest
    if (e.target.tagName === "BUTTON" && e.target.textContent === "Eliminar") {
        $.confirm({
            title: 'Eliminar del catálogo',
            autoClose: 'Cancelar|8000',
            buttons: {
                deleteUser: {
                    text: 'Sí, eliminar',
                    action: function () {
                        toastr.success("Se eliminó correctamente.")
                        xhr.open("POST","../../../model/catalog/delete.catalog.php",true)
                        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
                        xhr.addEventListener("load",() => {
                            fecthAll()
                        })
                        xhr.send(`id=${e.target.id}`)
                    }
                },
                Cancelar:{
                    title: 'nose',
                    cancelAction: function () {
                        $.alert('Cancelar');
                    }
                }
            }
        });
    }
})

// -----------------------Buscar----------------------------

let searchInput = document.getElementById("searchInput")
searchInput.addEventListener("keyup",()=>{

  const xhr = new XMLHttpRequest
  let searchValue = searchInput.value
  xhr.open("POST","../../../model/catalog/search.catalog.php",true)
  xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
  xhr.addEventListener("load",()=>{
    let catalogs = JSON.parse(xhr.responseText);
    let templateCatalogs = ''
    if (catalogs.length == 0) {
        templateCatalogs+=`
        <tr>
            <td colspan="6" align="center">No se encontraron resultados</td>
        </tr>
         ` 
        content_data.innerHTML = templateCatalogs    
    }else{
        catalogs.forEach(catalog => {
            templateCatalogs+=`
            <tr data-id="${catalog.id}">
                <td title="nombre">${catalog.nombre}</td>
                <td title="descripcion">${catalog.descripcion}</td>
                <td title="referencia">${catalog.referencia}</td>
                <td title="precio">${catalog.precio}</td>
                <td title="estado">${catalog.estado}</td>
                <td title="imagen">
                    <img class="img-fluid image" src="../../assets/images/shop_page/clothes/${catalog.imagenRuta}" alt="Card image cap ">
                </td>
                <td>
                    <button class="btn btn-danger btn-sm" id="${catalog.id}" type="submit">Eliminar</button>
                </td>
            </tr>
            `          
            content_data.innerHTML = templateCatalogs    
        });
    }
  })

  xhr.send(`search=${searchValue}`)
})