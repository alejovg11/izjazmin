

$(document).ready(function() {
    $("#registEmpleado").click(function() {
        
        $("#card-empleado").css({
            "right": "0",
            "transition": "0.6s"
        });
        $("#close").click(function() {
            $("#card-empleado").css({
                "right": "-100" + "%",
                "transition": "0.6s"
            });
        });
    });
});
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $(".icon-up").slideDown();
    } else {
        $(".icon-up").slideUp();
    }
});

$(".icon-up").click(function() {
    $("html,body").animate({ scrollTop: 0 }, 600);
});

let salir = document.getElementById('salir')
salir.addEventListener('click',()=>{
    localStorage.removeItem('usuario')
})

// -----------------------listar----------------------------

let content_data = document.getElementById("content_data")
const fecthAll = () => {
    let content_data = document.getElementById("content_data")
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/employees/read.employees.php",true)
    xhr.addEventListener("load",()=>{
        let employees = JSON.parse(xhr.responseText)
        let templateEmployees =""
        employees.forEach(employee => {
            templateEmployees+=`
                <tr data-id="${employee.id}">
                    <td title="nombre">${employee.nombre}</td>
                    <td title="apellido">${employee.apellido}</td>
                    <td title="telefono">${employee.telefono}</td>
                    <td title="direccion">${employee.direccion}</td>
                    <td title="ident">${employee.ident}</td>
                    <td>
                        <button class="btn btn-danger btn-sm" id="${employee.id}" type="submit">Eliminar</button>
                    </td>
                </tr>
                
            `
        })
        content_data.innerHTML=templateEmployees
    })
    xhr.send()
}
fecthAll()


// -----------------------Insertar----------------------------
let icon_spinner= document.getElementById('icon_spinner')
icon_spinner.style.display="none"

let form_employees = document.getElementById("form_employees")
let close = document.getElementById("close")
form_employees.addEventListener("submit",(e) => {
    e.preventDefault()
    const xhr = new XMLHttpRequest
    const formData = new FormData(form_employees)
    xhr.open("POST","../../../model/employees/insert.employees.php",true)
    xhr.addEventListener("load",() => {
        icon_spinner.style.display=""
        setTimeout(() => {
            fecthAll()
            icon_spinner.style.display="none"
        }, 2000);
        let r = xhr.responseText
        if(r == 2){
            toastr.info("Esta identificación ya existe, prueba con otro");
        }
        else{
            form_employees.reset();
            close.click()
            toastr.success("Se registró correctamente.")
        }
    })
    xhr.send(formData)
})

// -----------------------Eliminar----------------------------

content_data.addEventListener("click",(e)=>{
    const xhr = new XMLHttpRequest
    if (e.target.tagName === "BUTTON" && e.target.textContent === "Eliminar") {
        $.confirm({
            title: 'Eliminar empleado',
            autoClose: 'Cancelar|8000',
            buttons: {
                deleteUser: {
                    text: 'Sí, eliminar',
                    action: function () {
                        toastr.success("Se eliminó correctamente.")
                        xhr.open("POST","../../../model/employees/delete.employees.php",true)
                        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
                        xhr.addEventListener("load",() => {
                            fecthAll()
                        })
                        xhr.send(`id=${e.target.id}`)
                    }
                },
                Cancelar:{
                    title: 'nose',
                    cancelAction: function () {
                        $.alert('Cancelar');
                    }
                }
            }
        });
    }
})


// -----------------------Editar----------------------------
content_data.addEventListener("click",(e)=>{
    const xhr = new XMLHttpRequest
        if (e.target.tagName === "TD") {
            e.target.setAttribute("contenteditable", true)
            e.target.focus()
            e.target.setAttribute("spellcheck", false)
            
            e.target.addEventListener("keyup", e=> {
                let valorId = e.target.parentElement.dataset.id
                let nameUpdate = e.target.textContent
                let title = e.target.title

                if (e.keyCode === 13) {
                    save(nameUpdate,valorId,title)
                    e.target.blur()
                    // toastr.success("Se actualizó correctamente.")
                }    
            })
            const save = (valor,valorId,title) => {
                xhr.open("POST","../../../model/employees/update.employees.php",true)
                xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
                xhr.addEventListener("load",() => {
                    let r = xhr.responseText
                    if(r==2){
                        fecthAll()
                        toastr.info("Esta identificación ya existe, prueba con otro");
                    }
                    else{
                        close.click()
                        toastr.success("Se actualizó correctamente.")
                    }
                })
                xhr.send(`valor=${valor}&id=${valorId}&title=${title}`)
            }
        }
})

// -----------------------Buscar----------------------------

let searchInput = document.getElementById("searchInput")
searchInput.addEventListener("keyup",()=>{

  const xhr = new XMLHttpRequest
  let searchValue = searchInput.value
  xhr.open("POST","../../../model/employees/search.employees.php",true)
  xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
  xhr.addEventListener("load",()=>{
    let employees = JSON.parse(xhr.responseText);
    let templateEmployees = ''
    if (employees.length == 0) {
        templateEmployees+=`
        <tr>
            <td colspan="6" align="center">No se encontraron resultados</td>
        </tr>
         ` 
        content_data.innerHTML = templateEmployees    
    }else{
        employees.forEach(employee => {
            templateEmployees+=`
            <tr data-id="${employee.id}">
            <td title="nombre">${employee.nombre}</td>
            <td title="apellido">${employee.apellido}</td>
            <td title="telefono">${employee.telefono}</td>
            <td title="direccion">${employee.direccion}</td>
            <td title="ident">${employee.ident}</td>
            <td>
            <button class="btn btn-danger btn-sm" id="${employee.id}" type="submit">Eliminar</button>
            </td>
            </tr>
            `          
            content_data.innerHTML = templateEmployees    
        });
    }
  })

  xhr.send(`search=${searchValue}`)
})

// validate

const inputs = Array.from(document.querySelectorAll("[required]"))
inputs.forEach(input => {
    let div =   `<div class="invalid-feedback" id="${input.name}"></div>`
    input.parentElement.insertAdjacentHTML("beforeend",div)
        input.addEventListener("keyup",e=>{
            if (input.pattern) {
              let regex = new RegExp(input.pattern)
              if (!regex.exec(input.value)) {
                  document.querySelector(`#${input.name}`).innerHTML = `Introduce ${input.title}.`
                    input.classList.add("is-invalid")
                  
                }else{
                    input.classList.remove("is-invalid")
                    input.classList.add("is-valid")
                }   
            }
        })
});