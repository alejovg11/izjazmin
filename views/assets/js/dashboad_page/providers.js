$(document).ready(function() {
    $("#registProveedores").click(function() {
        $("#card-proveedores").css({
            "right": "0",
            "transition": "0.6s"
        });
        $("#close").click(function() {
            $("#card-proveedores").css({
                "right": "-100" + "%",
                "transition": "0.6s"
            });
        });
    });
});
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $(".icon-up").slideDown();
    } else {
        $(".icon-up").slideUp();
    }
});

$(".icon-up").click(function() {
    $("html,body").animate({ scrollTop: 0 }, 600);
});

let salir = document.getElementById('salir')
salir.addEventListener('click',()=>{
    localStorage.removeItem('usuario')
})

let content_data = document.getElementById("content_data")
const fecthAll = () => {
    let content_data = document.getElementById("content_data")
    const xhr = new XMLHttpRequest()
    xhr.open("GET","../../../model/providers/read.providers.php",true)
    xhr.addEventListener("load",()=>{
        let contVer = 0
        let providers = JSON.parse(xhr.responseText)
        let templateProviders =""
        providers.forEach(provider => {
            templateProviders+=`
                <tr data-id="${provider.id}">
                    <td title="nombre">${provider.nombre}</td>
                    <td title="nombre_ger">${provider.nombre_ger}</td>
                    <td title="correo_ger">${provider.correo_ger}</td>
                    <td title="telefono_ger">${provider.telefono_ger}</td>
                    <td class="d-flex">
                        <button class="btn btn-warning btn-sm" type="button" data-id="${contVer++}" data-toggle="modal" data-target="#exampleModal${provider.id}">Ver detalle</button>
                        <button class="btn btn-danger btn-sm ml-2" id="${provider.id}" type="submit">Eliminar</button>
                    </td>
                </tr>
            `
            content_data.innerHTML=templateProviders
        })
        const openModal = (e) => {
            let parentElement = e.target.parentElement
                        
            let providerDetalle = providers[e.target.dataset.id]            
            let template =
            `
            <div class="modal fade" id="exampleModal${providerDetalle.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Detalle Proveedor</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                <div class="container-fluid">
                <div class="row">
                  <div class="col-md-6">
                    <p class="m-0 p-0">Nombre empresa:</p>
                    <p class="text-muted">${providerDetalle.nombre.toUpperCase()}</p>
                    <hr>
                    <p class="m-0 p-0">Correo empresa:</p>
                    <p class="text-muted">${providerDetalle.correo}</p>
                    <hr>
                    <p class="m-0 p-0">Teléfono empresa:</p>
                    <p class="text-muted">${providerDetalle.telefono}</p>
                    <hr>
                    <p class="m-0 p-0">Nit:</p>
                    <p class="text-muted">${providerDetalle.nit}</p>       
                  </div>
                  <div class="col-md-6">
                    <p class="m-0 p-0">Nombre gerente:</p>
                    <p class="text-muted">${providerDetalle.nombre_ger}</p>
                    <hr>
                    <p class="m-0 p-0">Correo gerente:</p>
                    <p class="text-muted">${providerDetalle.correo_ger}</p>
                    <hr>
                    <p class="m-0 p-0">Teléfono gerente:</p>
                    <p class="text-muted">${providerDetalle.telefono_ger}</p>
                    <hr>
                    <p class="m-0 p-0">Dirección:</p>
                    <p class="text-muted">${providerDetalle.direccion}</p>         
                  </div>
                </div>
              </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-outline-mybtn" data-dismiss="modal">Cerrar</button>
                  </div>
              </div>
            </div>
          </div>`
            parentElement.insertAdjacentHTML("beforeend",template)
        }

        let buttons = document.querySelectorAll("button")
        Array.from(buttons).forEach(element => {
            if (element.textContent === "Ver detalle") {
                element.addEventListener("click", openModal)
            }
        })
    })
    xhr.send()
}
fecthAll()



// -----------------------Insertar----------------------------
let icon_spinner= document.getElementById('icon_spinner')
icon_spinner.style.display="none"
let form_providers = document.getElementById("form_providers")
let close = document.getElementById("close")
form_providers.addEventListener("submit",(e) => {
    e.preventDefault()
    const xhr = new XMLHttpRequest
    const formData = new FormData(form_providers)
    xhr.open("POST","../../../model/providers/insert.providers.php",true)
    xhr.addEventListener("load",() => {
        icon_spinner.style.display=""
        setTimeout(() => {
            fecthAll()
            icon_spinner.style.display="none"
        }, 2000);
        let r = xhr.responseText
        if(r == 2){
            toastr.info("Algunos campos ya existen, prueba con otro");
        }
        else{
            form_providers.reset();
            close.click()
            toastr.success("Se registró correctamente.")
        }
    })
    xhr.send(formData)
})

// -----------------------Eliminar----------------------------

content_data.addEventListener("click",(e)=>{
    const xhr = new XMLHttpRequest
    if (e.target.tagName === "BUTTON" && e.target.textContent === "Eliminar") {
        $.confirm({
            title: 'Eliminar proveedor',
            autoClose: 'Cancelar|8000',
            buttons: {
                deleteUser: {
                    text: 'Sí, eliminar',
                    action: function () {
                        toastr.success("Se eliminó correctamente.")
                        xhr.open("POST","../../../model/providers/delete.providers.php",true)
                        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
                        xhr.addEventListener("load",() => {
                            fecthAll()
                        })
                        xhr.send(`id=${e.target.id}`)
                    }
                },
                Cancelar:{
                    title: 'nose',
                    cancelAction: function () {
                        $.alert('Cancelar');
                    }
                }
            }
        });
    }
})

// -----------------------Editar----------------------------

content_data.addEventListener("click",(e)=>{
    const xhr = new XMLHttpRequest
        if (e.target.tagName === "TD") {
            e.target.setAttribute("contenteditable", true)
            e.target.focus()
            e.target.setAttribute("spellcheck", false)
            
            e.target.addEventListener("keyup", e=> {
                let valorId = e.target.parentElement.dataset.id
                let nameUpdate = e.target.textContent
                let title = e.target.title

                if (e.keyCode === 13) {
                    save(nameUpdate,valorId,title)
                    e.target.blur()
                }    
            })
            const save = (valor,valorId,title) => {
                xhr.open("POST","../../../model/providers/update.providers.php",true)
                xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
                xhr.addEventListener("load",() => {
                    let r = xhr.responseText
                    if(r==2){
                        fecthAll()
                        toastr.info("Esta información ya existe, prueba con otro");
                    }
                    else{
                        close.click()
                        toastr.success("Se actualizó correctamente.")
                    }
                })
                xhr.send(`valor=${valor}&id=${valorId}&title=${title}`)
            }
        }
})

// -----------------------Buscar----------------------------

let searchInput = document.getElementById("searchInput")
searchInput.addEventListener("keyup",()=>{

  const xhr = new XMLHttpRequest
  let searchValue = searchInput.value
  xhr.open("POST","../../../model/providers/search.providers.php",true)
  xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
  xhr.addEventListener("load",()=>{
    let providers = JSON.parse(xhr.responseText);
    let templateProviders = ''
    if (providers.length == 0) {
        templateProviders+=`
        <tr>
            <td colspan="6" align="center">No se encontraron resultados</td>
        </tr>
         ` 
        content_data.innerHTML = templateProviders    
    }else{
        providers.forEach(provider => {
            templateProviders+=`
            <tr data-id="${provider.id}">
                <td title="nombre">${provider.nombre}</td>
                <td title="nombre_ger">${provider.nombre_ger}</td>
                <td title="correo_ger">${provider.correo_ger}</td>
                <td title="telefono_ger">${provider.telefono_ger}</td>
                <td class="d-flex">
                    <button class="btn btn-warning btn-sm" type="button" data-toggle="modal" data-target="#exampleModal${provider.id}">Ver detalle</button>
                    <button class="btn btn-danger btn-sm ml-2" id="${provider.id}" type="submit">Eliminar</button>
                </td>
            </tr>
            `          
            content_data.innerHTML = templateProviders    
        });
    }
  })

  xhr.send(`search=${searchValue}`)
})

// validate

const inputs = Array.from(document.querySelectorAll("[required]"))
inputs.forEach(input => {
    let div =   `<div class="invalid-feedback" id="${input.name}"></div>`
    input.parentElement.insertAdjacentHTML("beforeend",div)
        input.addEventListener("keyup",e=>{
            if (input.pattern) {
              let regex = new RegExp(input.pattern)
              if (!regex.exec(input.value)) {
                  document.querySelector(`#${input.name}`).innerHTML = `Introduce ${input.title}.`
                    input.classList.add("is-invalid")
                }else{
                    input.classList.remove("is-invalid")
                    input.classList.add("is-valid")
                }   
            }
        })
});