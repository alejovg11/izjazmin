let localstorage = JSON.parse(localStorage.getItem("usuario"))

let nombre = document.getElementById("nombre")
let apellido = document.getElementById("apellido")
let correo = document.getElementById("correo")
let identificacion = document.getElementById("identificacion")
let direccion = document.getElementById("direccion")
let telefono = document.getElementById("telefono")
let contrasenaActual = document.getElementById("contrasenaActual")
let button = document.getElementById("button")

nombre.value = localstorage.nombre
apellido.value = localstorage.apellido
correo.value = localstorage.correo
identificacion.value = localstorage.ident
direccion.value = localstorage.direccion
telefono.value = localstorage.telefono
contrasenaActual.value = localstorage.contrasena
button.setAttribute("name", localstorage.id) 


let form_profile = document.getElementById("form_profile")
form_profile.addEventListener("submit",(e)=>{
    e.preventDefault()
    const formData = new FormData(form_profile)
    const xhr = new XMLHttpRequest()
    xhr.open("POST","../../../model/profile/edit.profile.php",true)
    xhr.addEventListener("load",()=>{
    })
    xhr.send(formData,)
})
