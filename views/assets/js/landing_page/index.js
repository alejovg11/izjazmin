$(window).on("scroll", function() {
    if($(window).scrollTop()) {
          $('nav').addClass('black');
          // navbar-light(color de letra de la barra de navegacion)
          $('nav').removeClass('navbar-light');
          $('nav').addClass('navbar-dark');
          $("#image_logo").attr("src", "views/assets/images/landing_page/logo_blanco.png");

    }
    else {
          $('nav').removeClass('black');
          $('nav').css('transition','1s');
          $('nav').removeClass('navbar-dark');
          $('nav').addClass('navbar-light');
          $("#image_logo").attr("src", "views/assets/images/landing_page/logo.png");

    }
})
$(document).ready(function () {
$(".boton").click(function () {
    $('html,body').animate({scrollTop:"500px"},1500);
});
});

// -----------------------sign up----------------------------

let form_signup = document.getElementById("form_signup")
let close = document.getElementById("close")

form_signup.addEventListener("submit",(e) => {
    e.preventDefault()
    const xhr = new XMLHttpRequest
    const formData = new FormData(form_signup)    
    xhr.open("POST","model/client/insert.client.php",true)
    xhr.addEventListener("load",() => {
        form_signup.reset()
        let r = xhr.responseText
        console.log(r);			
        if(r==2){
            toastr.info("Este correo ya existe, prueba con otro");
        }
        else{
            form_signup.reset();
            close.click()
            toastr.success("Se registró correctamente.")
        }
    })
    xhr.send(formData)
})

// -----------------------log in----------------------------

let form_login = document.getElementById("form_login")
let close2 = document.getElementById("close2")

form_login.addEventListener("submit",(e) => {    
    e.preventDefault()
    const xhr = new XMLHttpRequest
    const formData = new FormData(form_login)
    xhr.open("POST","model/login/validate.login.php",true)
    xhr.addEventListener("load",() => {
        form_login.reset()
        let response = JSON.parse(xhr.responseText);
        console.log(response);
        if (response[0]) {
            toastr.success("Ingresó correctamente.")
            close2.click()
            setTimeout(() => {
                document.location.href = response[1];
                localStorage.setItem("usuario",JSON.stringify(response[2]))
            }, 1000);
            // alert("ingreso")
        }else{
            toastr.error(response[1])
        }
    })
    xhr.send(formData)
})



// validate

const inputs = Array.from(document.querySelectorAll("[required]"))

inputs.forEach(input => {
    let div =   `<div class="invalid-feedback" id="${input.name}"></div>`
    input.parentElement.insertAdjacentHTML("beforeend",div)
        input.addEventListener("keyup",e=>{
            if (input.pattern) {
              let regex = new RegExp(input.pattern)
              if (!regex.exec(input.value)) {
                  document.querySelector(`#${input.name}`).innerHTML = `Introduce ${input.title}.`
                    input.classList.add("is-invalid")
                  
                }else{
                    input.classList.remove("is-invalid")
                    input.classList.add("is-valid")
                }   
            }
        })
});




