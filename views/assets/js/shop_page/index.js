toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-full-width",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "6500",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
let content_data = document.getElementById("content_data")
    const xhr = new XMLHttpRequest()
    xhr.open("GET", "../../../model/shop/read.shop.php", true)
    xhr.addEventListener("load", () => {
        let productsData = JSON.parse(xhr.responseText)
        console.log(productsData);
        productsData.map((p) => {
          p['cantidad']=0;
        })
        let sidebar = document.getElementById("sidebar"),
            sidebarContainer = document.getElementById("sidebarContainer"),
            close = document.getElementById("close"),
            content_data = document.getElementById("content_data"),
            template = "",                         
            btn_cantidad = document.getElementById("btn_cantidad"),
            cantidad = document.getElementById("cantidad")

        close.addEventListener("click",()=>{
            sidebar.classList.add("close")
        })
        btn_cantidad.addEventListener("click",()=>{
            sidebar.classList.remove("close")
        })
        productsData.forEach(element => {
            template+=`
            <div class="col-sm-12 col-lg-4 mb-4">
                <div class="card shadow">
                      <div class="image-card">
                          <img class="card-img-top image" src="../../assets/images/shop_page/clothes/${element.imagenRuta}" alt="Card image cap ">
                      </div>
                    <div class="card-body">
                        <p class="p-0 my-2">${element.nombre.toUpperCase()}</p>
                        <h6 class="font-weight-bold mb-3">$${element.precio}</h6>
                        <hr>
                        <div class="d-flex justify-content-center">
                          <button class="btn btn-outline-mybtn m-1" data-id="${element.id}" type="button" data-toggle="modal" data-target="#exampleModal${element.id}">Ver detalle</button>
                          <button class="btn btn-mybtn m-1" data-id="${element.id}" type="button" data-status="card">Agregar al carrito</button>
                        </div>
                    </div>
                </div>
            </div>
            `
        })
        content_data.insertAdjacentHTML("beforeend",template)
        
        class Producto {
            constructor(product, cantidad){
                this.product = product
                this.cantidad = cantidad
            }
        }
        class Carrito{
            constructor(key){
                this.key = key
                if (!localStorage.getItem(key)) {
                    localStorage.setItem(key, JSON.stringify([]))
                }
                    this.addProduct = this.addProduct.bind(this)
                    this.verDetalle = this.verDetalle.bind(this)
            }

            addProduct(e) {
              if(e.target.dataset.status === "modal"){              
                let encontrarObjetoInventario = productsData.filter(p => p.id === e.target.id)
                let cantidadInventario = parseInt(encontrarObjetoInventario[0]["cantidadInventario"]);
                let cantidadDetalle = e.target.parentElement.parentElement
                .querySelector(".modal-body")
                .querySelector(".container-fluid")
                .querySelector(".row")
                .querySelector(".col-md-7")
                .querySelector(".row")
                .querySelector(".col-md-6")
                .nextElementSibling
                .querySelector(`#cantidadDetalle${e.target.id}`)

                if (!cantidadDetalle.value || cantidadDetalle.value > cantidadInventario || cantidadDetalle.value == 0) {
                    toastr.warning("No contamos en el inventario con la cantidad que requieres, hemos actualizado con la cantidad disponible.")
                    cantidadDetalle.value = cantidadInventario
                    cantidadDetalle.style.border = "1px solid #d63031"
                    cantidadDetalle.style.backgroundColor = "rgba(250, 177, 160,0.1)"
                } else {
                  let products = JSON.parse(localStorage.getItem(this.key))
                  let indice = productsData.findIndex(product => product.id === e.target.dataset.id)
                  let product = new Producto(productsData[indice], cantidadDetalle.value)                
                  e.target.previousElementSibling.click()
                  sidebar.classList.remove("close")
                  let nohay = document.getElementById("nohay"),
                  container_button = document.getElementById("container_button");
                  
                  nohay.style.display = "none"
                  container_button.style.display = "block"
                  
                  let validarExistenciaProducto = products.filter((p => p['product'].id === product['product'].id))                
                  if (validarExistenciaProducto.length) {
                    let indiceProductoActualizar = products.findIndex((p) => p['product'].id === product['product'].id)               
                    products[indiceProductoActualizar] = product;
                    this.editarElemento(products[indiceProductoActualizar])
                  } else {
                    products.push(product);
                    this.renderProduct(product["product"], product["cantidad"])
                  }
                  localStorage.setItem(this.key, JSON.stringify(products));
                  cantidad.innerHTML = products.length
                }
              } else if (e.target.dataset.status === "card") {
                let products = JSON.parse(localStorage.getItem(this.key))
                let indice = productsData.findIndex(product => product.id === e.target.dataset.id)
                let product = new Producto(productsData[indice], 1)                
                sidebar.classList.remove("close")
                let nohay = document.getElementById("nohay"),
                container_button = document.getElementById("container_button");
                nohay.style.display = "none"
                container_button.style.display = "block"
  
                let validarExistenciaProducto = products.filter((p => p['product'].id === product['product'].id))                
                if (validarExistenciaProducto.length) {
                    let indiceProductoActualizar = products.findIndex((p) => p['product'].id === product['product'].id)               
                    products[indiceProductoActualizar] = product;
                    this.editarElemento(products[indiceProductoActualizar])
                } else {
                  products.push(product);
                  this.renderProduct(product["product"], product["cantidad"])
                }
                localStorage.setItem(this.key, JSON.stringify(products));
                cantidad.innerHTML = products.length
              }
            }

            editarElemento(producto){
              document.getElementById(`productoCarrito-${producto.product.id}`).remove();
              this.renderProduct(producto.product, producto.cantidad);
            }  
            
            verDetalle(e){
              let productsData = JSON.parse(xhr.responseText)
              let indice = productsData.findIndex(p=>p.id === e.target.dataset.id)

              let productDetalle = productsData[indice],                
                    template =`
                    <div class="modal fade" id="exampleModal${productDetalle.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Detalle producto</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                        <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-5">
                            <img class="card-img-top image" src="../../assets/images/shop_page/clothes/${productDetalle.imagenRuta}" alt="Card image cap ">
                          </div>
                          <div class="col-md-7">
                            <h5>${productDetalle.nombre.toUpperCase()}</h5>
                            <p class="m-0 p-0">Precio:</p>
                            <p class="text-muted">$${productDetalle.precio}</p>
                            <p class="m-0 p-0">Descripción:</p>
                            <p class="text-muted">${productDetalle.descripcion}</p>
                            <p class="m-0 p-0">Referencia:</p>
                            <p class="text-muted">${productDetalle.referencia}</p>
                            <div class="row">
                              <div class="col-md-6">
                                <select class="form-control mr-2" name="detalle" required>
                                  <option value="">${productDetalle.talla}</option>
                                </select>
                              </div>
                              <div class="col-md-6">
                                  <input type="number" class="form-control" placeholder="Cantidad" value="1" id="cantidadDetalle${productDetalle.id}" min="1" max="10" pattern="^[0-9]+" required>
                                <span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-outline-mybtn" data-dismiss="modal">Cerrar</button>
                          <button type="button" class="btn btn-mybtn" data-id="${productDetalle.id}" id="${productDetalle.id}" data-status="modal">Agregar al carrito</button>
                          </div>
                      </div>
                    </div>
                  </div>`
                content_data.insertAdjacentHTML("beforeend", template)
                let buttonsModal = document.querySelectorAll(".modal button")        
                Array.from(buttonsModal).forEach(buttonModal => {
                    if (buttonModal.textContent === "Agregar al carrito") {
                        buttonModal.addEventListener("click", this.addProduct)                        
                    }
                })

                // Validación para no aceptar numeros negativos o con caracteres especiales
                const campoNumerico = document.getElementById(`cantidadDetalle${productDetalle.id}`);
                campoNumerico.addEventListener('keydown', function(evento) {
                  const teclaPresionada = evento.key;
                  const teclaPresionadaEsUnNumero =
                    Number.isInteger(parseInt(teclaPresionada));

                  const sePresionoUnaTeclaNoAdmitida = 
                    teclaPresionada != 'ArrowDown' &&
                    teclaPresionada != 'ArrowUp' &&
                    teclaPresionada != 'ArrowLeft' &&
                    teclaPresionada != 'ArrowRight' &&
                    teclaPresionada != 'Backspace' &&
                    teclaPresionada != 'Delete' &&
                    teclaPresionada != 'Enter' &&
                    !teclaPresionadaEsUnNumero;
                  const comienzaPorCero = 
                    campoNumerico.value.length === 0 &&
                    teclaPresionada == 0;

                  if (sePresionoUnaTeclaNoAdmitida || comienzaPorCero) {
                    evento.preventDefault(); 
                  }

                })
          
            }
            
            renderProduct(product,cantidadPrenda){
            
                let templateProduct = `
                <div class="d-flex justify-content-md-around align-items-center card-product-cart" id="productoCarrito-${product.id}">
                  <img class="img-fluid imageDetalle" src="../../assets/images/shop_page/clothes/${product.imagenRuta}" alt="Card image cap ">
                  <div>
                      <h5>${product.nombre.toUpperCase()}</h5>
                      <p class="my-0 py-0">Referencia: <span class="text-muted">${product.referencia}</span></p>
                      <p class="my-0 py-0">Precio: <span class="text-muted">${product.precio}</span></p> 
                      <p class="my-0 py-0">Talla: <span class="text-muted">${product.talla}</span></p> 
                      <p>Cantidad: <span class="text-muted">${cantidadPrenda}</span></p> 
                  </div>
                  <hr>
                </div>
                `
                sidebarContainer.insertAdjacentHTML("beforeend",templateProduct)       
              
            }

            render(){
              let products = JSON.parse(localStorage.getItem(this.key)),
              nohay = document.getElementById("nohay"),
              container_button = document.getElementById("container_button");
              cantidad.innerHTML = products.length
                if (products.length == 0) {
                  nohay.style.display = "block"
                  container_button.style.display = "none"
                }else{
                  nohay.style.display = "none"
                  container_button.style.display = "block"
                  products.forEach(product => this.renderProduct(product['product'], product['cantidad']))
                }
            }
  
            actions(){
                let buttons = document.querySelectorAll("button")
                Array.from(buttons).forEach(element => {
                  if  (element.textContent === "Ver detalle") {
                        element.addEventListener  ("click", this.verDetalle)
                      }
                })

                let buttonsCard = document.querySelectorAll(".card button")        
                Array.from(buttonsCard).forEach(buttonCard => {
                    if (buttonCard.textContent === "Agregar al carrito") {
                      buttonCard.addEventListener("click", this.addProduct)                        
                    }
                })
            }
        }
        const carrito = new Carrito("carrito")
        carrito.render()
        carrito.actions()
    })        
    xhr.send()

