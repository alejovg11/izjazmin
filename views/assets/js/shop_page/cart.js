let content_data = document.getElementById('content_data'),
	precio_total = document.getElementById('precio_total'),
	precio_subtotal = document.getElementById('precio_subtotal'),
	products = JSON.parse(localStorage.getItem('carrito')),
	productRender = products.map(p => p['product']),
	cantidades = products.map(p => p['product']['cantidad'] = p['cantidad']),
	locationUrl = location.href,
    content_session = document.getElementById('content_session'),
    vaciarCarrito = document.getElementById('vaciarCarrito'),
    content_cart = document.getElementById('content_cart');



function carritoVacio() {
    content_cart.innerHTML = `
    <div class="p-absolute">
        <h2 class="mb-4">Carrito de compra</h2>
        <img src="../../assets/images/shop_page/shopper.svg" class="my-4" alt="" width="150">
        <h4 class="">Tu carrito está vacío</h4>
        <a role="button" href="/isjazmin/views/modules/shop_page" class="btn btn-outline-mybtn mt-4">Sigue comprando</a>
    </div>` 
}    

//-----------------renderizacion del producto-----------------

if (JSON.parse(localStorage.getItem('carrito')).length) {
    let container = ''
    productRender.forEach(product => {
        container += ` 
            <div class="col-lg-8 mb-3 order-md-1">
                <div class="card shadow">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-2 content_line d-flex align-items-center">
                                <img src="../../assets/images/shop_page/clothes/${product.imagenRuta}" alt="" class="img-fluid image-carro">
                            </div>
                            <div class="col-lg-5">
                                <h4>${product.nombre}</h4>
                                <p class="my-2 py-0">Referencia: <span class="text-muted">${product.referencia}</span></p>
                                <p class="my-2 py-0">Talla: <span class="text-muted">${product.talla}</span></p>
                                <p class="my-2 py-0">Precio unidad: <span class="text-muted" data-precioUnitario=${product.precio}>${product.precio}</span></p>
                                <p class="my-2 py-0">Cantidad: <input type="number" class="text-muted input-disabled" value="${product.cantidad}" data-cantidad=${product.id} disabled></p>
                                <a href="" class="text-danger" data-id="${product.id}">Eliminar</a>
                                <span class="text-muted mx-1">|</span>
                                <a href="" class="text-warning" data-id="${product.id}">Editar</a>
                            </div>
                            <div class="col-lg-5">
                                <h4 class="mb-4 text-right" data-precio="${product.precio * product.cantidad}">${product.precio * product.cantidad} COP</h4> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `
    });
    content_data.insertAdjacentHTML('beforeend', container)
    
    // ---------------- para calcular la suma total--------------

    let preciosTotales = document.querySelectorAll("[data-precio]"),
        valorDeTodo = 0
        preciosTotales.forEach(element => {
        valorDeTodo+= parseInt(element.dataset.precio);
    });
    precio_subtotal.textContent = valorDeTodo
    precio_total.textContent = valorDeTodo
    

    // --------------Eliminar producto del carrito--------------

    let buttonsEliminar = document.querySelectorAll("a")
    buttonsEliminar.forEach(button => {
        button.addEventListener('click', (e) => {
            if (button.textContent === "Eliminar") {   
                e.preventDefault()
                let products = JSON.parse(localStorage.getItem('carrito')),
                productArray = products.map(p => p['product']),
                toRemove = productArray.findIndex(product => product.id.toString() === e.target.dataset.id);                 
                products.splice(toRemove, 1)
                localStorage.setItem('carrito', JSON.stringify(products))
                    e.target
                    .parentElement
                    .parentElement
                    .parentElement
                    .parentElement
                    .parentElement
                    .remove();
                
                    // ---------------- para calcular la suma total--------------

                    let preciosTotales = document.querySelectorAll("[data-precio]")
                    valorDeTodo = 0;
                    preciosTotales.forEach(element => {
                        valorDeTodo+= parseInt(element.dataset.precio);
                    });
                    precio_subtotal.textContent = valorDeTodo
                    precio_total.textContent = valorDeTodo
                    cantidad.innerHTML = products.length
                    if (!products.length) {
                        carritoVacio()
                    }
            }
        })
    })

    // --------------Editar producto del carrito--------------

    let buttonsEditar = document.querySelectorAll("a")
    buttonsEditar.forEach(button => {
        button.addEventListener('click', (e) => {
            if (button.textContent === "Editar") {   
                e.preventDefault()
                button.textContent = "Aceptar cambios"
                let cantidadEditable = e.target
                    .parentElement
                    .parentElement
                    .parentElement
                    .parentElement.
                    querySelectorAll("[data-cantidad]")[0]
                    cantidadEditable.removeAttribute("disabled");
                    cantidadEditable.classList.remove("input-disabled");
                    cantidadEditable.classList.add("form-control-cantidad");
    
            }else if(button.textContent === "Aceptar cambios"){
                e.preventDefault()
                button.textContent = "Editar"
                let cantidadEditable = e.target
                    .parentElement
                    .parentElement
                    .parentElement
                    .parentElement
                    .querySelectorAll("[data-cantidad]")[0]
                    cantidadEditable.setAttribute("disabled", true);
                    cantidadEditable.classList.add("input-disabled");
                    cantidadEditable.classList.remove("form-control-cantidad");

                    let nuevaCantidad = cantidadEditable.value,
                    products = JSON.parse(localStorage.getItem('carrito')),
                    // obtengo el indice para actulizarlo en el localStorage
                    indice = products.findIndex(product => product["product"].id === cantidadEditable.dataset.cantidad);
                    products[indice].cantidad = nuevaCantidad;
                    localStorage.setItem('carrito', JSON.stringify(products))

                    let precioTotalActualizado = e.target
                    .parentElement
                    .parentElement
                    .parentElement
                    .parentElement
                    .querySelectorAll("[data-precio]")[0]

                    let precioUnitario = parseInt(e.target
                    .parentElement
                    .parentElement
                    .parentElement
                    .parentElement
                    .querySelectorAll("[data-precioUnitario]")[0]
                    .textContent)

                    precioTotalActualizado.textContent = `${nuevaCantidad * precioUnitario} COP`
                    precioTotalActualizado.dataset.precio = nuevaCantidad * precioUnitario

                    // ---------------- para calcular la suma total--------------
                    let preciosTotales = document.querySelectorAll("[data-precio]")
                    valorDeTodo = 0;
                    preciosTotales.forEach(element => {
                        valorDeTodo+= parseInt(element.dataset.precio);
                    });
                    precio_subtotal.textContent = valorDeTodo
                    precio_total.textContent = valorDeTodo

            }
        })
    })

// -----------------------------------------------------

    // --------------insert de un JSON--------------

    let compra = document.getElementById('compra')
    compra.addEventListener('click', (e) => {
        if (e.target.dataset.status === "false") {
            toastr.info("No has iniciado sesión.");
        }else{
            e.preventDefault()
            let carrito = JSON.parse(localStorage.getItem('carrito'));
            let usuario = JSON.parse(localStorage.getItem('usuario'));
            let dataSend = JSON.stringify(carrito.concat(usuario));
            const xhr = new XMLHttpRequest();
            xhr.open("POST","../../../model/shop/insert.shop.php",true)
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.addEventListener("load",() => {
                let r = xhr.responseText
            })
            xhr.send(dataSend)
            toastr.success("Gracias por su compra.");
            setTimeout(() => { 
                location.href = `../../../views/modules/shop_page/orders.php?id=${JSON.parse(localStorage.getItem("usuario"))["id"]}`
            }, 1000);
        }
    })

} else {  
    carritoVacio()
}

let cantidad = document.getElementById("cantidad")
cantidad.innerHTML = products.length

vaciarCarrito.addEventListener('click', (e) => {
    e.preventDefault()
    let products = JSON.parse(localStorage.getItem('carrito'))
    products = []
    localStorage.setItem('carrito', JSON.stringify(products))
    content_data.innerHTML = ""
    carritoVacio()
    cantidad.innerHTML = products.length
})

// function mediaQuery(breakpoint) {
//     if (breakpoint.matches) { 
//     let card_compra = document.getElementById("card_compra")
//         card_compra.classList.remove("fixed")
//     } else {
//         card_compra.classList.add("fixed")
//     }
//   }
  
//   let breakpoint = window.matchMedia("(min-width: 700px)")
//   mediaQuery(breakpoint)
  