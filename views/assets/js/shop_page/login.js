// -----------------------log in----------------------------

let form_login = document.getElementById("form_login")

form_login.addEventListener("submit",(e) => {
    e.preventDefault()
    const xhr = new XMLHttpRequest
    const formData = new FormData(form_login)
    xhr.open("POST","../../../model/loginUsers/validate.login.php",true)
    xhr.addEventListener("load",() => {
        form_login.reset()
        let response = JSON.parse(xhr.responseText);
        console.log(response);
        if (response[0]) {
            document.location.href = response[1];
            localStorage.setItem("usuario", JSON.stringify(response[2]))
        }else{
            toastr.error(response[1])
        }
    })
    xhr.send(formData)
})