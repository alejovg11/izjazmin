// -----------------------sign up----------------------------

let form_signup = document.getElementById("form_signup")
let close = document.getElementById("close")

form_signup.addEventListener("submit",(e) => {
    e.preventDefault()
    const xhr = new XMLHttpRequest
    const formData = new FormData(form_signup)    
    xhr.open("POST","../../../model/client/insert.client.php",true)
    xhr.addEventListener("load",() => {
        form_signup.reset()
        let r = xhr.responseText
        console.log(r);			
        if(r==2){
            toastr.info("Este correo ya existe, prueba con otro");
        }
        else{
            form_signup.reset();
            toastr.success("Se registró correctamente.")
            setTimeout(() => { 
                location.href="login.php"
            }, 1400);
        }
    })
    xhr.send(formData)
})