(function(){
	var scale = 1.05;
	var container = document.getElementsByClassName("")[0];
	getContainerDimensions();
	var offset = ((scale - Math.floor(scale)) / 2) * 100;

	scale = "scale(" + scale + ")";

	window.addEventListener("resize", function() {
		getContainerDimensions()
	});

	container.addEventListener("mousemove", function(e) {
		moveBgImage(e);
	});

	function getContainerDimensions() {
		width = container.offsetWidth;
		height = container.offsetHeight;
	}

	function moveBgImage(e) {
		// Find center of container
		var pageX = e.pageX - (width / 2);
		var pageY = e.pageY - (height / 2);

		// Calculate movement
		var newX = -(pageX / ((width/2)/offset));
		var newY = -(pageY / ((height/2)/offset));

		// Move bgimage
		container.style.webkitTransform = "translate(" + newX + "%," + newY + "%) " + scale;
		container.style.MozTransform = "translate(" + newX + "%," + newY + "%) " + scale;
		container.style.msTransform = "translate(" + newX + "%," + newY + "%) " + scale;
		container.style.OTransform = "translate(" + newX + "%," + newY + "%) " + scale;
		container.style.transform = "translate(" + newX + "%," + newY + "%) " + scale;
	}

})();
