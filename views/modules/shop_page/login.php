<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Iniciar sesión</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/shop_page/login.css">
</head>

<body>
    <div class="shape">
    </div>
    <div class="container mx-auto">
        <div class="row d-flex justify-content-center align-items-center" id="body">
            <div class="col-md-6">
                <div class="card shadow animated fadeIn">
                    <div class="card-body">
                        <form id="form_login" class="row">
                            <div class="col-lg-12">
                                <div class="d-flex justify-content-between align-items-center">
                                    <h4 class="mb-4">Iniciar sesión</h4>
                                    <img src="../../assets/images/landing_page/logo.png" alt="" class="image_logo" id="image_logo">
                                </div>
                                <label class="text-paragraft col-form-label">Correo electrónico:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                        <i class="fas fa-envelope"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control input-gray" title="tu correo" placeholder="Correo" name="correo" required>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <label class="text-paragraft col-form-label">Contraseña:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-lock"></i>
                                        </span>
                                    </div>
                                    <input type="password" class="form-control input-gray" title="tu contraseña" placeholder="Contraseña" name="contrasena" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="col-lg-12 text-center">
                                <button type="submit" class="btn btn-mybtn btn-block">Ingresar</button>
                                <div class="s-mb-2">
                                    <p class="p-0 m-0 mt-3">¿No tienes una cuenta?
                                        <a href="signup.php">Registrarse</a>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../assets/js/wow.min.js "></script>
    <script>
        new WOW().init();
    </script>
    <script src="../../assets/js/jquery.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/shop_page/login.js"></script>
    <script src="../../assets/js/toastr.js"></script>
</body>

</html>