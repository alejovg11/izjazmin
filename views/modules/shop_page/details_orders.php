<?php
  include('../../../model/conn.model.php');
  include('../../../model/shop/read.details_orders.php');
  session_start();
  if (!isset($_SESSION["client"])){
      header ("Location: ./index.php");
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detalle de mi pedido</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/shop_page/index.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
</head>
<body> 
    <nav class="navbar navbar-expand-lg navbar-light d-flex justify-content-between mycontainer">
        <div>
            <form class="form-inline ml-2">
                <a class="navbar-brand" href="../shop_page">
                <img src="../../assets/images/landing_page/logo.png" alt="" width="80" id="image_logo">
                </a>
                <div class="input-group ">
                    <input type="text" class="form-control " placeholder="Buscar">
                    <div class="input-group-append ">
                        <span class="input-group-text icon-search">
                            <i class="fas fa-search "></i>
                        </span>
                    </div>
                </div>
            </form>
        </div>
        <div>
            <ul class="navbar-nav">
                <li class="nav-item dropdown d-flex align-items-center" id="content_session">
                    <?php if (!isset($_SESSION["client"])): ?>
                        <a role="button" href="cart.php" class="btn btn-outline-mybtn btn-sm mr-2" id="btn_cantidad">
                            <i class="fas fa-shopping-cart icon-user"></i>
                            <span class="badge badge-pill badge-dark" id="cantidad"></span>
                        </a>
                        <a class="btn btn-outline-mybtn btn-sm" href="login.php" role="button">                            
                            Ingresar
                        </a>
                    <?php else: ?>
                        <a role="button" href="cart.php" class="btn btn-outline-mybtn btn-sm mr-2" id="btn_cantidad">
                            <i class="fas fa-shopping-cart icon-user"></i>
                            <span class="badge badge-pill badge-dark" id="cantidad"></span>
                        </a>
                        <div class="btn-group user-border d-flex align-items-center">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="username" role="button">
                                <div class="avatar text-white d-flex align-items-center justify-content-center">
                                    <?php echo $_SESSION["client"]["nombre"][0] ?>
                                </div> 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left" aria-labelledby="dropdownMenuButton">
                                <div class="pl-4 pr-4">
                                    <p>
                                    <?php echo $_SESSION["client"]["nombre"] ?> <?php echo $_SESSION["client"]["apellido"] ?>
                                    <span class="text-muted"> <?php echo $_SESSION["client"]["correo"] ?></span>
                                    </p>
                                </div>
                                <a class="dropdown-item" href="orders.php?id=<?php echo $_SESSION["client"]["id"] ?>">Historial de pedidos</a>
                                <a class="dropdown-item" href="myData.php?id=<?php echo $_SESSION["client"]["id"] ?>">Mis datos</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="../../modules/shop_page/cerrarTienda.php" role="button" id="closeSession">
                                    Salir
                                </a>
                            </div>
                        </div>
                    <?php endif ?>
                </li>
            </ul>
        </div>
    </nav>
    <!-- navbar2 -->
    <!-- <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-shop">
        <div class="d-sm-inline-block navbar-collapse justify-content-center">
            <ul class="navbar-nav navbar-shop-items">
                <li class="nav-item items-shop">
                    <a href="" class="nav-link ">
                        HOMBRE
                    </a>
                </li>
                <li class="nav-item items-shop">
                    <a href="" class="nav-link ">
                        MUJER
                    </a>
                </li>
                <li class="nav-item items-shop">
                    <a href="" class="nav-link ">
                        NIÑO
                    </a>
                </li>
            </ul>
        </div>
    </nav> -->
    <div class="container mt-4">
        <div class="d-flex align-items-center my-3">
            <a href="" id="urlDetallepedido">
                <i class="fas fa-arrow-left text-muted" style="font-size:1.2rem;"></i>
            </a>
            <h2 class="ml-4">Detalle de mi pedido</h2>
        </div>
        <table class="table table-striped table-sm animated fadeIn">
            <thead>
                <tr>
                    <th>Imagen</th>
                    <th>Producto</th>
                    <th>Talla</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                </tr>
            </thead>
            <tbody id="content_data"> 
                <?php foreach ($result as $data): ?>
                    <tr>
                        <td><img src="../../assets/images/shop_page/clothes/<?php echo $data['imagenRuta'] ?>" alt="" width="70"></td>
                        <td> <?php echo $data['nombre'] ?></td>
                        <td> <?php echo $data['talla_nombre'] ?></td>
                        <td> <?php echo $data['cantidad'] ?></td>                            
                        <td> <?php echo $data['precio'] ?></td>                            
                    </tr>
                <?php endforeach?>
            </tbody>
        </table>
</div>
<script src="../../assets/js/jquery.min.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/js/shop_page/details_orders.js"></script>
</body>
</html>