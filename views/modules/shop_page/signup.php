<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/shop_page/signup.css">
</head>

<body>
    <div class="shape">
    </div>
    <div class="container mx-auto">
        <div class="row d-flex justify-content-center align-items-center" id="body">
            <div class="col-md-6">
                <div class="card shadow">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4 class="mb-4">Registro</h4>
                            <img src="../../assets/images/landing_page/logo.png" alt="" class="image_logo" id="image_logo">
                        </div>
                        <form class="row animated fadeIn" id="form_signup">
                            <div class="col-lg-6">
                                <label class="text-paragraft col-form-label">Nombre:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-user"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control input-gray" title="tu nombre" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" placeholder="Nombre" name="nombre" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="text-paragraft col-form-label">Apellido:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-user"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control input-gray" title="tu apellido" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,64}" placeholder="Apellido" name="apellido" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="text-paragraft col-form-label">Identificación:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-id-card"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control input-gray" title="tu documento" pattern="[0-9]{8,10}" maxlength="10" placeholder="Identificacion" name="identificacion" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="text-paragraft col-form-label">Teléfono:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-phone"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control input-gray" title="tu telefono" pattern="[0-9]{7}" maxlength="7" placeholder="Telefono" name="telefono" required>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <label class="text-paragraft col-form-label">Dirección:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-home"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control input-gray" title="tu dirección" placeholder="Direccion" name="direccion" required>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <label class="text-paragraft col-form-label">Correo electrónico:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-envelope"></i>
                                        </span>
                                    </div>
                                    <input type="email" class="form-control input-gray" title="tu correo" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Correo" name="correo2" required>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <label class="text-paragraft col-form-label">Contraseña:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-lock"></i>
                                        </span>
                                    </div>
                                    <input type="password" class="form-control input-gray" title="tu contraseña" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*)(?=.*[a-z]).*$" placeholder="Contraseña" name="contrasena2" required>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <p class="text-muted text-info-validate p-0 m-0">Utiliza ocho caracteres como mínimo con una combinación de letras, números o simbolos</p>
                            </div>
                            <div class="col-md-12">
                            <hr>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-mybtn btn-block">Registrarme</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../assets/js/wow.min.js "></script>
    <script>
        new WOW().init();
    </script>
    <script src="../../assets/js/jquery.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/shop_page/signup.js"></script>
    <script src="../../assets/js/toastr.js"></script>
</body>

</html>