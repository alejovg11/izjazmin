<?php
  include('../../../model/conn.model.php');
  include('../../../model/shop/read.details_orders.php');
  session_start();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Carrito de compra</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/shop_page/cart.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light d-flex justify-content-between">
        <div>
            <form class="form-inline ml-2">
                <a class="navbar-brand" href="/isjazmin/views/modules/shop_page/">
                    <img src="../../assets/images/landing_page/logo.png" alt="" width="80" id="image_logo">
                </a>
                <div class="input-group ">
                    <input type="text" class="form-control " placeholder="Buscar">
                    <div class="input-group-append ">
                        <span class="input-group-text icon-search">
                            <i class="fas fa-search "></i>
                        </span>
                    </div>
                </div>
            </form>
        </div>
        <div>
            <ul class="navbar-nav">
                <li class="nav-item dropdown d-flex align-items-center" id="content_session">
                    <?php if (!isset($_SESSION["client"])): ?>
                        <a role="button" href="#" class="btn btn-outline-mybtn btn-sm mr-2" id="btn_cantidad">
                            <i class="fas fa-shopping-cart icon-user"></i>
                            <span class="badge badge-pill badge-dark" id="cantidad"></span>
                        </a>
                        <a class="btn btn-outline-mybtn btn-sm" href="login.php" role="button">                            
                            Ingresar
                        </a>
                    <?php else: ?>
                        <a role="button" href="#" class="btn btn-outline-mybtn btn-sm mr-2" id="btn_cantidad">
                            <i class="fas fa-shopping-cart icon-user"></i>
                            <span class="badge badge-pill badge-dark" id="cantidad"></span>
                        </a>
                        <div class="btn-group user-border d-flex align-items-center">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="username" role="button">
                                <div class="avatar text-white d-flex align-items-center justify-content-center">
                                    <?php echo $_SESSION["client"]["nombre"][0] ?>
                                </div> 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left" aria-labelledby="dropdownMenuButton">
                                <div class="pl-4 pr-4">
                                    <p>
                                    <?php echo $_SESSION["client"]["nombre"] ?> <?php echo $_SESSION["client"]["apellido"] ?>
                                    <span class="text-muted"> <?php echo $_SESSION["client"]["correo"] ?></span>
                                    </p>
                                </div>
                                <a class="dropdown-item" href="orders.php?id=<?php echo $_SESSION["client"]["id"] ?>">Historial de pedidos</a>
                                <a class="dropdown-item" href="myData.php?id=<?php echo $_SESSION["client"]["id"] ?>">Mis datos</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="../../modules/shop_page/cerrarTienda.php" role="button" id="closeSession">
                                    Salir
                                </a>
                            </div>
                        </div>
                    <?php endif ?>
                </li>
            </ul>
        </div>
    </nav>
    <!-- navbar2 -->
    <!-- <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-shop">
        <div class="d-sm-inline-block navbar-collapse justify-content-center">
            <ul class="navbar-nav navbar-shop-items">
                <li class="nav-item items-shop">
                    <a href="" class="nav-link ">
                        HOMBRE
                    </a>
                </li>
                <li class="nav-item items-shop">
                    <a href="" class="nav-link ">
                        MUJER
                    </a>
                </li>
                <li class="nav-item items-shop">
                    <a href="" class="nav-link ">
                        NIÑO
                    </a>
                </li>
            </ul>
        </div>
    </nav> -->
    <div class="content">
        <div class="container py-4" id="content_cart">
            
            <h2 class="mb-4">Carrito de compra</h2>
            <div class="row animated fadeIn" id="content_data">
                <div class="col-lg-4 order-md-2 fixed" id="card_compra">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item font-weight-bold">Resumen de tu pedido</li>
                        <li class="list-group-item">
                            <div class="d-flex justify-content-between">
                                <span class="text-paragraft">Subtotal</span>
                                <span class="text-paragraft" id="precio_subtotal"></span>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="d-flex justify-content-between">
                                <span class="text-paragraft">Total</span>
                                <span class="text-paragraft" id="precio_total"></span>
                            </div>
                            <?php if (!isset($_SESSION["client"])): ?>
                                <button class="btn btn-outline-mybtn btn-block my-4" id="compra" data-status="false">Procesar compra</button>     
                            <?php else: ?>
                                <button class="btn btn-outline-mybtn btn-block my-4" id="compra" data-status="true">Procesar compra</button>
                            <?php endif ?>
                            <a href="" class="text-danger" id="vaciarCarrito">Vaciar carrito de compra</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebarShow close" id="sidebar">
        <div class="container container-sidebar" id="sidebarContainer">
            <div class="d-flex justify-content-md-between align-items-center mb-3">
                <i class="fas fa-times icon-close" id="close"></i>
                <h6 class="p-0 m-0 text-muted">Detalle del carrito</h6>
            </div>
            <div class="container_button" id="container_button">
                <a href="cart.php" class="btn btn-mybtn btn-block btn_check">Revisar</a>
            </div>
        </div>
        <h5 id="nohay" class="carritoVacio text-muted">Tu carrito está vacío</h5>
    </div>
    <script src="../../assets/js/wow.min.js "></script>
    <script>
        new WOW().init();
    </script>
    <script src="../../assets/js/jquery.min.js"></script>
    <script src="../../assets/js/popper.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/shop_page/cart.js"></script>
    <script src="../../assets/js/toastr.js"></script>
</body>

</html>