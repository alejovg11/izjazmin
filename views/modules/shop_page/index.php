<?php
  $mysqli = new mysqli('localhost', 'root', '', 'appbd');
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>isJazmin</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../assets/css/shop_page/index.css">
        <link rel="stylesheet" href="../../assets/css/animate.css">
        <link rel="stylesheet" href="../../assets/css/hover-min.css">
        <link rel="stylesheet" href="../../assets/css/toastr.css">
    </head>

    <body>
        <!-- navbar -->
            <nav class="navbar navbar-expand-lg navbar-light d-flex justify-content-between mycontainer">
                <div>
                    <form class="form-inline ml-2">
                        <a class="navbar-brand" href="../shop_page">
                            <img src="../../assets/images/landing_page/logo.png" alt="" width="80" id="image_logo">
                        </a>
                        <div class="input-group ">
                            <input type="text" class="form-control " placeholder="Buscar">
                            <div class="input-group-append ">
                                <span class="input-group-text icon-search">
                                    <i class="fas fa-search "></i>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div>
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown d-flex align-items-center" id="content_session">
                            <?php if (!isset($_SESSION["client"])): ?>
                                <a role="button" href="#" class="btn btn-outline-mybtn btn-sm mr-2" id="btn_cantidad">
                                    <i class="fas fa-shopping-cart icon-user"></i>
                                    <span class="badge badge-pill badge-dark" id="cantidad"></span>
                                </a>
                                <a class="btn btn-outline-mybtn btn-sm" href="login.php" role="button">                            
                                    Ingresar
                                </a>
                            <?php else: ?>
                                <a role="button" href="#" class="btn btn-outline-mybtn btn-sm mr-2" id="btn_cantidad">
                                    <i class="fas fa-shopping-cart icon-user"></i>
                                    <span class="badge badge-pill badge-dark" id="cantidad"></span>
                                </a>
                                <div class="btn-group user-border d-flex align-items-center">
                                    <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="username" role="button">
                                        <div class="avatar text-white d-flex align-items-center justify-content-center">
                                            <?php echo $_SESSION["client"]["nombre"][0] ?>
                                        </div> 
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left" aria-labelledby="dropdownMenuButton">
                                        <div class="pl-4 pr-4">
                                            <p>
                                            <?php echo $_SESSION["client"]["nombre"] ?> <?php echo $_SESSION["client"]["apellido"] ?>
                                            <span class="text-muted"> <?php echo $_SESSION["client"]["correo"] ?></span>
                                            </p>
                                        </div>
                                        <a class="dropdown-item" href="orders.php?id=<?php echo $_SESSION["client"]["id"] ?>">Historial de pedidos</a>
                                        <a class="dropdown-item" href="myData.php?id=<?php echo $_SESSION["client"]["id"] ?>">Mis datos</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item text-danger" href="../../modules/shop_page/cerrarTienda.php" role="button" id="closeSession">
                                            Salir
                                        </a>
                                    </div>
                                </div>
                            <?php endif ?>
                        </li>
                    </ul>
                </div>
            </nav>
                    <!-- navbar2 -->
            <!-- <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-shop">
                <div class="d-sm-inline-block navbar-collapse justify-content-center">
                    <ul class="navbar-nav navbar-shop-items">
                        <li class="nav-item items-shop">
                            <a href="" class="nav-link ">
                                HOMBRE
                            </a>
                        </li>
                        <li class="nav-item items-shop">
                            <a href="" class="nav-link ">
                                MUJER
                            </a>
                        </li>
                        <li class="nav-item items-shop">
                            <a href="" class="nav-link ">
                                NIÑO
                            </a>
                        </li>
                    </ul>
                </div>
            </nav> -->
        <div class="content">
            <div id="carouselExampleControls" class="carousel" data-ride="carousel">
                <div class="carousel-inner slide2">
                    <div class="carousel-item active carousel-shop">
                        <img class="d-block w-100" src="../../assets/images/shop_page/carousel/1.jpg" alt="First slide">
                        <div class="carousel-caption d-md-block caption ">
                            <h1 class="wow bounceInUp">¡Bienvenidos!</h1>
                        </div>
                    </div>
                    <div class="carousel-item carousel-shop">
                        <img class="d-block w-100" src="../../assets/images/shop_page/carousel/2.jpg" alt="Second slide">
                        <div class="carousel-caption  d-md-block caption">
                            <h1 class="wow shake titles">¡Dilo con tu camiseta! </h1>
                        </div>
                    </div>
                    <div class="carousel-item carousel-shop">
                        <img class="d-block w-100" src="../../assets/images/shop_page/carousel/3.jpg" alt="Four slide">
                        <div class="carousel-caption  d-md-block caption">
                            <h1 class="wow bounceInDown">¡Exclusiva ropa!</h1>
                        </div>
                    </div>
                    <div class="carousel-item carousel-shop">
                        <img class="d-block w-100" src="../../assets/images/shop_page/carousel/4.jpg" alt="Five slide">
                        <div class="carousel-caption  d-md-block caption">
                            <h1 class="wow bounceInRight">¡Nuevas colecciones!</h1>
                        </div>
                    </div>
                    <div class="carousel-item carousel-shop">
                        <img class="d-block w-100" src="../../assets/images/shop_page/carousel/5.jpg" alt="Six slide">
                        <div class="carousel-caption  d-md-block caption">
                            <h1 class="wow bounceInLeft">¡Promociones!</h1>
                        </div>
                    </div>
                    <div class="carousel-item carousel-shop">
                        <img class="d-block w-100" src="../../assets/images/shop_page/carousel/6.jpg" alt="Six slide">
                        <div class="carousel-caption  d-md-block caption">
                            <h1 class="wow bounceInLeft">¡Promociones!</h1>
                        </div>
                    </div>
                    <div class="carousel-item carousel-shop">
                        <img class="d-block w-100" src="../../assets/images/shop_page/carousel/8.jpg" alt="Six slide">
                        <div class="carousel-caption  d-md-block caption">
                            <h1 class="wow bounceInLeft">¡Promociones!</h1>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                </div>
            </div>
            <!-- contenido -->
            <div class="container">
                <div class="row py-4">
                    <div class="col-lg-2">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="categoria d-flex justify-content-lg-between">
                                    <h6>CATEGORIAS</h6>
                                    <i class="fas fa-caret-up icon-shop"></i>
                                </div>
                                <ul class="itemscateg">
                                    <li class="items-categ">Camisetas</li>
                                    <li class="items-categ">Polos</li>
                                    <li class="items-categ">Busos</li>
                                    <li class="items-categ">Camisas</li>
                                    <li class="items-categ">Shorts</li>
                                    <li class="items-categ">Jeans y pantalones</li>
                                    <li class="">Joggers</li>
                                </ul>
                            </div>
                            <div class="col-lg-12">
                                <div class="talla d-flex justify-content-lg-between my-4">
                                    <h6>TALLA</h6>
                                    <i class="fas fa-caret-up icon-shop"></i>
                                </div>
                                <div class="row mt-4 talla-content">
                                    <div class="col-lg-4 ">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                            <label class="custom-control-label" for="customCheck1">XL</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                                            <label class="custom-control-label" for="customCheck2">S</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 ">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck3">
                                            <label class="custom-control-label" for="customCheck3">M</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 ">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck4">
                                            <label class="custom-control-label" for="customCheck4">XL</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 ">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck5">
                                            <label class="custom-control-label" for="customCheck5">XXS</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 ">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck6">
                                            <label class="custom-control-label" for="customCheck6">L</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 ">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck7">
                                            <label class="custom-control-label" for="customCheck7">XS</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-4 ">
                                <h6>PRECIO</h6>
                                <select class="form-control">
                                    <option value=" ">Selec. tu rango</option>
                                    <option value=" ">$40.000 - $50.000</option>
                                    <option value=" ">$60.000 - $70.000</option>
                                    <option value=" ">$80.000 - $90.000</option>
                                    <option value=" ">$100.000 - $150.000</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="row" id ="content_data"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sidebarShow close" id="sidebar">
            <div class="container container-sidebar" id="sidebarContainer">
                <div class="d-flex justify-content-md-between align-items-center mb-3">
                    <i class="fas fa-times icon-close" id="close"></i>
                    <h6 class="p-0 m-0 text-muted">Detalle del carrito</h6>
                </div>
                <div class="container_button" id="container_button">
                    <a href="cart.php" class="btn btn-mybtn btn-block btn_check">Revisar</a>
                </div>
            </div>
                <h5 id="nohay" class="carritoVacio text-muted">Tu carrito está vacío</h5>
        </div>
        <script src="../../assets/js/wow.min.js "></script>
        <script>
            new WOW().init();
        </script>
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/toastr.js"></script>
        <script src="../../assets/js/shop_page/index.js"></script>

    </body>

</html>