<?php 
    session_start();
    if (!isset($_SESSION["user"])){
    header ("Location: ../../../index.php");
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Pedidos</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Comfortaa|Poiret+One|Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../assets/css/dashboard_page/orders.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <?php 
                    include "./navbar.php";
                ?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    <div class="d-flex justify-content-between my-3">
                        <h2>Pedidos</h2>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm animated fadeIn">
                            <thead>
                                <tr>
                                    <th>Número pedido</th>
                                    <th>Cliente</th>
                                    <th>Estado</th>
                                    <th>Fecha</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody id="content_data"> 
                          
                            </tbody>
                        </table>
                    </div>
                </main>
                <i class="fas fa-arrow-up icon-up"></i>
            </div>
        </div>
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/dashboad_page/orders.js"></script>
        <script src="../../assets/js/toastr.js"></script>
        <script src="../../assets/js/jquery-confirm.js"></script>
    </body>

</html>