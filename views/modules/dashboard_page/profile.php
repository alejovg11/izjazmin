<?php 
    session_start();
    if (!isset($_SESSION["user"])){
    header ("Location: ../../../index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Perfil</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Comfortaa|Poiret+One|Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../assets/css/dashboard_page/profile.css">
    </head>

    <body>
        <!-- navbar -->
        <nav class="navbar navbar-expand-lg fixed-top navbar-light">
                <a href="index_admin.php" class="navbar-brand">isJazmin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                 <span class="navbar-toggler-icon"></span>
                 </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item item-app">
                            <a href="profile.jsp" class="nav-link link-app">
                                <i class="fas fa-user-circle icon-app " data-toggle="tooltip" data-placement="bottom" title=""> </i>
                            </a>
                        </li>
                        <li class="nav-item item-app">
                            <a href="#" class="nav-link  ">
                                <span class="">|</span>
                            </a>
                        </li>
                        <li class="nav-item  item-app">
                            <a href="../../Logout" class="nav-link link-app e">
                                <i class="fas fa-power-off"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        <div class="container">
            <div class="row flex-main animated fadeIn">
                <div class="col-lg-4">
                    <div class="card shadow">
                        <div class="card-body text-center ">
                            <h5 class="text-center text-muted">Tu perfil</h5>
                            <div class="avatar my-4"></div>
                            <h6 class="text-muted"></h6>
                            <h6 class="text-muted font-weight-normal"></h6>
                            <div class="mt-5 mb-3 btn-group">
                                <button class="btn btn-mybtn btn-sm" type="button">Actualizar foto</button>
                                <button class="btn btn-outline-secondary btn-sm" type="button">Eliminar foto</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="card shadow">
                        <div class="card-header">
                            Editar mi información personal
                        </div>
                        <div class="card-body ">
                            <form class="row container mx-auto" id="form_profile">
                                <div class="form-group z-index col-lg-6">
                                    <label for="" class="text-paragraft col-form-label">Nombre:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-user"></i>
                                            </span>
                                        </div>
                                        <input value="" type="text" class="form-control input-gray" placeholder="Nombre" name="nombre" id="nombre" required>
                                    </div>
                                </div>
                                <div class="form-group z-index col-lg-6">
                                    <label for="" class="text-paragraft col-form-label">Apellidos:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-user"></i>
                                            </span>
                                        </div>
                                        <input value="" type="text" class="form-control input-pass-login input-gray" placeholder="Apellidos" name="apellido" id="apellido" required>
                                    </div>
                                </div>
                                <div class="form-group z-index col-lg-6">
                                    <label for="" class="text-paragraft col-form-label">Correo:</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-envelope"></i>
                                            </span>
                                        </div>
                                        <input value="" type="text" class="form-control input-gray" placeholder="Correo" name="correo" id="correo" required>
                                    </div>
                                </div>
                                <div class="form-group z-index col-lg-6">
                                    <label for="" class="text-paragraft col-form-label">Identificación:</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-envelope"></i>
                                            </span>
                                        </div>
                                        <input value="" type="text" class="form-control input-gray" placeholder="Identificación" name="ident" id="identificacion" required>
                                    </div>
                                </div>
                                <div class="form-group z-index col-lg-6">
                                    <label for="" class="text-paragraft col-form-label ">Dirrección:</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-directions"></i>
                                            </span>
                                        </div>
                                        <input value="" type="text" class="form-control input-gray" placeholder="Dirrección" name="direccion" id="direccion" required>
                                    </div>
                                </div>
                                <div class="form-group z-index col-lg-6">
                                    <label for="" class="text-paragraft col-form-label">Telefono:</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-phone"></i>
                                            </span>
                                        </div>
                                        <input value="" type="text" class="form-control input-pass-login input-gray" placeholder="Telefono" name="telefono" id="telefono" required>
                                        <hr>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <hr>
                                </div>
                                <div class="form-group z-index col-lg-6">
                                    <label for="" class="text-paragraft col-form-label">Contraseña actual:</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-lock"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-pass-login input-gray" placeholder="Contraseña actual" name="cActual" id="contrasenaActual" required>
                                    </div>
                                </div>
                                <div class="form-group z-index col-lg-6">
                                    <label for="" class="text-paragraft col-form-label">Nueva contraseña:</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-lock"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-pass-login input-gray" placeholder="Nueva contraseña" name="nContra" required>
                                        <hr>
                                    </div>
                                </div>
                                <div class="col-lg-12 mt-4">
                                    <button class="btn btn-mybtn btn-block" type="submit" name="button" id="button">Actualizar información</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/dashboad_page/profile.js"></script>
    </body>

</html>