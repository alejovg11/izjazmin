<?php 
    session_start();
    if (!isset($_SESSION["user"])){
    header ("Location: ../../../index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Proveedores</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Comfortaa|Poiret+One|Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../assets/css/dashboard_page/providers.css">
        <link rel="stylesheet" href="../../assets/css/toastr.css">

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <?php 
                    include "./navbar.php";
                ?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    <div class="d-flex justify-content-between my-3">
                        <h2>Proveedores</h2>
                        <button class="btn btn-outline-mybtn btn-sm" id="registProveedores">Registrar proveedor</button>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm animated fadeIn">
                            <thead>
                                <tr>
                                    <th>Nombre empresa</th>
                                    <th>Nombre gerente</th>
                                    <th>Correo gerente</th>
                                    <th>Telefono gerente</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody id="content_data">
                             
                            </tbody>
                        </table>
                        <div class="col-md-12 text-center my-4">
                            <i class="fas fa-spinner fa-spin icon-load" id="icon_spinner"></i>
                        </div>
                    </div>
                </main>
                <div class="col-lg-6" id="card-proveedores">
                    <div class="card shadow">
                        <div class="card-header">
                            Registrar Proveedor
                            <button class="close" id="close">
                                <span class="fas fa-arrow-right"></span>
                            </button>
                        </div>
                        <div class="card-body">
                            <form class="row container mx-auto" id="form_providers">
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Nombre empresa:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-building"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title="la empresa" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" placeholder="Nombre" name="nombre" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Correo empresa:</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-envelope"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title="el correo" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Correo" name="correo" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Nit:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-building"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title="el nit" pattern="[0-9]{10}" maxlength="10" placeholder="Nit" name="nit" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Dirección:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-directions"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title="la dirección" pattern="[A-Z a-z 0-9]{5,40}" placeholder="Direccion" name="direccion" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Teléfono empresa:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title ="el telefono" pattern="[0-9]{7}" maxlength="7" placeholder="Telefono" name="telefono" required>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <hr>
                                </div>
                                <div class="col-lg-6">

                                    <label class="text-paragraft col-form-label">Nombre gerente:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-user"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title="el nombre del gerente" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" placeholder="Nombre completo" name="nombre_ger" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Teléfono gerente:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title ="el telefono del gerente" pattern="[0-9]{7}" maxlength="7" placeholder="Telefono del gerente" name="telefono_ger" required>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <label class="text-paragraft col-form-label">Correo gerente:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-envelope"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title="el correo del gerente" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Correo del gerente" name="correo_ger" required>
                                    </div>
                                </div>
                                <div class="col-lg-12 mt-4">
                                    <button class="btn btn-mybtn btn-block" type="submit">Completar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <i class="fas fa-arrow-up icon-up"></i>
            </div>
        </div>
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/dashboad_page/providers.js"></script>
        <script src="../../assets/js/toastr.js"></script>
        <script src="../../assets/js/jquery-confirm.js"></script>

    </body>

</html>