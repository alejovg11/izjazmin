
<?php
  $mysqli = new mysqli('localhost', 'root', '', 'isjazmin');
    session_start();
    if (!isset($_SESSION["user"])){
    header ("Location: ../../../index.php");
    }
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Insumos</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Comfortaa|Poiret+One|Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../assets/css/dashboard_page/supplies.css">
        <link rel="stylesheet" href="../../assets/css/toastr.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <?php 
                    include "./navbar.php";
                ?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    <div class="d-flex justify-content-between my-3">
                        <h2>Insumos</h2>
                        <div>
                            <button class="btn btn-outline-mybtn btn-sm" id="añadirInsumos">Añadir insumo</button>
                            <button class="btn btn-outline-mybtn btn-sm" id="registInsumos">Pedir Insumos</button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm animated fadeIn">
                            <thead>
                                <tr>
                                    <th>Nombre insumo</th>
                                    <th>Cantidad</th>
                                    <th>Unidad de medida</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody id="content_data"></tbody>
                        </table>
                    </div>
                    <div class="d-flex justify-content-between my-3">
                        <h2>Pedidos de insumos</h2>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>Insumo comprado</th>
                                    <th>Empresa proveedora</th>
                                    <th>Cantidad comprada</th>
                                    <th>Nombre unidad</th>
                                    <th>Valor compra</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody id="content_data2"></tbody>
                        </table>
                        <div class="col-md-12 text-center my-4">
                            <i class="fas fa-spinner fa-spin icon-load" id="icon_spinner"></i>
                        </div>
                    </div>
                </main>
                
                <!-----------FORMULARIOS DE "PEDIR INSUMO"----------->
                <div class="col-lg-6" id="card-insumos">
                    <div class="card shadow">
                        <div class="card-header">
                            Pedir Insumos
                            <button class="close" id="close">
                                <span class="fas fa-arrow-right"></span>
                            </button>
                        </div>
                        <div class="card-body">
                            <!-----------INICIO FORMULARIO 1----------->
                            <form class="row container mx-auto" id="form_orders">
                                <div class="col-lg-6">
                                    <label for="" class="text-paragraft col-form-label">Insumo:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-th-list"></i>
                                            </span>
                                        </div>
                                        <select class="form-control" name="insumo" required>
                                            <option value="">Seleccione:</option>
                                            <?php
                                            $query = $mysqli -> query ("SELECT * FROM insumos");
                                            while ($valores = mysqli_fetch_array($query)) {
                                                echo '<option value="'.$valores[id].'">'.$valores[nombre].'</option>';
                                            }
                                            ?>
                                        </select>  
                                    </div>
                                </div>
                                
                                <div class="col-lg-6">
                                    <label for="" class="text-paragraft col-form-label">Seleccionar unidad de medida:</label>
                                    <select class="form-control" name="unidades" required>
                                        <option value="">Seleccione:</option>
                                        <?php
                                        $query = $mysqli -> query ("SELECT * FROM unidades");
                                        while ($valores = mysqli_fetch_array($query)) {
                                            echo '<option value="'.$valores[id].'">'.$valores[nombre_unidad].'</option>';
                                        }
                                        ?>
                                    </select>  
                                </div>
                                        
                                <div class="col-lg-6">
                                    <label for="" class="text-paragraft col-form-label">Empresa proveedora:</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-building"></i>
                                            </span>
                                        </div>
                                        <select class="form-control" name="empresa" required>
                                            <option value="">Seleccione:</option>
                                            <?php
                                            $query = $mysqli -> query ("SELECT * FROM proveedores");
                                            while ($valores = mysqli_fetch_array($query)) {
                                                echo '<option value="'.$valores[id].'">'.$valores[nombre].'</option>';
                                            }
                                            ?>
                                        </select>      
                                    </div>
                                </div>
                                
                                <div class="col-lg-6">
                                    <label for="" class="text-paragraft col-form-label ">Cantidad:</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-arrows-alt"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title ="la cantidad" pattern="[0-9]" placeholder="Cantidad" name="cantidad" required>
                                    </div>
                                </div>
                                <div class="form-group z-index col-lg-12">
                                    <label for="" class="text-paragraft col-form-label">Precio:</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-pass-login input-gray" title ="el precio" pattern="[0-9]" placeholder="Valor" name="precio" required>
                                        <hr>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <hr>
                                </div>
                                <div class="col-lg-12 mt-4">
                                    <button class="btn btn-mybtn btn-block" type="submit">Completar</button>
                                </div>
                            </form>
                            <!-----------FIN FORMULARIO 1----------->

                        </div>
                    </div>
                </div>
                <!-----------FIN FORMULARIOS DE "PEDIR INSUMO"----------->
                
                
                <!-----------INICIO FORMULARIOS DE "AÑADIR INSUMO"----------->
                <div class="col-lg-6" id="card-insumos-añadir">
                    <div class="card shadow">
                        <div class="card-header">
                            Añadir Insumos
                            <button class="close" id="close-añadir">
                                <span class="fas fa-arrow-right"></span>
                            </button>
                        </div>
                        <div class="card-body">
                            <form class="row container mx-auto" id="form_supplies">
                                <div class="col-lg-12 form-group">
                                    <label for="" class="text-paragraft col-form-label">Nombre insumo:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-th-list"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title="el nombre del insumo" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{3,48}" placeholder="Nombre insumo" name="nombreInsumo" required>
                                    </div>
                                </div>
                                <div class="col-lg-12 form-group">
                                    <label for="" class="text-paragraft col-form-label">Cantidad:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-th-list"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title="la cantidad" pattern="[0-9]" placeholder="Cantidad" name="cantidad2" required>
                                    </div>
                                </div>
                                <div class="col-lg-12 form-group">
                                    <label for="" class="text-paragraft col-form-label">Unidad de medida</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-sort-numeric-down"></i>
                                            </span>
                                        </div>
                                        <select class="form-control" name="unidad" required>
                                            <option value="">Seleccione:</option>
                                            <?php
                                            $query = $mysqli -> query ("SELECT * FROM unidades");
                                            while ($valores = mysqli_fetch_array($query)) {
                                                echo '<option value="'.$valores[id].'">'.$valores[nombre_unidad].'</option>';
                                            }
                                            ?>
                                        </select>       
                                    </div>
                                </div>
                                <div class="col-lg-12 mt-4">
                                    <button class="btn btn-mybtn btn-block" type="submit" id="btn-registrar-insumo">Registrar</button>
                                </div>
                            </form>       
                            <!-----------FIN FORMULARIOS DE "AÑADIR INSUMO"----------->
                        </div>
                        </form>
                    </div>
                </div>
                <i class="fas fa-arrow-up icon-up"></i>
            </div>
            <i class="fas fa-arrow-up icon-up"></i>
        </div>
    <script src="../../assets/js/jquery.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/dashboad_page/supplies.js"></script>
    <script src="../../assets/js/toastr.js"></script>
    <script src="../../assets/js/jquery-confirm.js"></script>


</body>

</html>