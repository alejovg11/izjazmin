<?php 
    session_start();
    if (!isset($_SESSION["user"])){
    header ("Location: ../../../index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Menu</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/dashboard_page/index_admin.css">
    <link rel="stylesheet" href="../../assets/css/hover-min.css">

</head>
<body>
    <!-- navbar -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-light">
        <a href="./index_admin.php" class="navbar-brand">
        <img src="../../assets/images/landing_page/logo.png" alt="" width="80" id="image_logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
         </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item item-app">
                    <a href="profile.php" class="nav-link link-app">
                    <span id="user"></span>
                        <i class="fas fa-user-circle icon-app " data-toggle="tooltip" data-placement="bottom" title=""> </i>
                    </a>
                </li>
                <li class="nav-item item-app">
                    <a href="#" class="nav-link">
                        <span class="">|</span>
                    </a>
                </li>
                <li class="nav-item  item-app">
                    <a href="./cerrar.php" class="nav-link link-app" id="off">
                        <i class="fas fa-power-off text-danger"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div class="row flex-main animated fadeIn">
            <div class="col-lg-4">
                <a href="employees.php" class="link">
                    <div class="card shadow card-main text-center hvr-shrink">
                        <div class="card-body">
                            <div class="box_icon">
                                <i class="fas fa-users icon"></i>
                            </div>
                            <h6 class="lead text-muted paragraft">Empleados</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 ">
                <a href="providers.php" class="link ">
                    <div class="card shadow card-main text-center hvr-shrink">
                        <div class="card-body ">
                            <div class="box_icon">
                                <i class="fas fa-dolly icon"></i>
                            </div>
                            <h6 class="lead text-muted paragraft">Proveedores</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="inventory.php" class="link">
                    <div class="card shadow card-main text-center hvr-shrink">
                        <div class="card-body ">
                            <div class="box_icon">
                                <i class="fas fa-box-open icon"></i>
                            </div>
                            <h6 class="lead text-muted paragraft">Inventario</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 ">
                <a href="supplies.php" class="link">
                    <div class="card shadow card-main text-center hvr-shrink">
                        <div class="card-body ">
                            <div class="box_icon">
                                <i class="fas fa-th-list icon"></i>
                            </div>
                            <h6 class="lead text-muted paragraft">Insumos</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 ">
                <a href="client.php" class="link">
                    <div class="card shadow card-main text-center hvr-shrink">
                        <div class="card-body ">
                            <div class="box_icon">
                                <i class="fas fa-user icon"></i>
                            </div>
                            <h6 class="lead text-muted paragraft">Clientes</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="orders.php" class="link ">
                    <div class="card shadow card-main text-center hvr-shrink">
                        <div class="card-body ">
                                <div class="box_icon">
                                    <i class="fas fa-map-marker-alt icon"></i>
                                </div>
                            <h6 class="lead text-muted paragraft">Pedidos</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="usuarios.php" class="link">
                    <div class="card shadow card-main text-center hvr-shrink">
                        <div class="card-body">
                            <div class="box_icon">
                                <i class="fas fa-user icon"></i>
                            </div>
                            <h6 class="lead text-muted paragraft">Usuarios</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="catalog.php" class="link">
                    <div class="card shadow card-main text-center hvr-shrink">
                        <div class="card-body">
                            <div class="box_icon">
                                <i class="fas fa-tshirt icon"></i>
                            </div>
                            <h6 class="lead text-muted paragraft">Catálogo</h6>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
 </div>
 <script src="../../assets/js/dashboad_page/index_admin.js"></script>
</body>

</html>