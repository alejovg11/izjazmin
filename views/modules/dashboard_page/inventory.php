<?php
  session_start();
  if (!isset($_SESSION["user"])){
      header ("Location: ../../../index.php");
  }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Inventario</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../assets/css/dashboard_page/inventory.css">
        <link rel="stylesheet" href="../../assets/css/toastr.css">
        <link rel="stylesheet" href="../../assets/css/dashboard_page/croppie.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <?php 
                    include "./navbar.php";
                ?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    <div class="d-flex justify-content-between my-3">
                        <h2>Inventario</h2>
                        <div class="btn-group dropleft">
                            <button type="button" class="btn-outline-mybtn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Registrar...
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" id="añadirProducto">Nuevo producto</a>
                                <a class="dropdown-item" id="añadirTipo">Nuevo tipo de producto</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm animated fadeIn">
                            <thead>
                                <tr>
                                    <th>Nombre prenda</th>
                                    <th>Cantidad</th>
                                    <th>Referencia producto</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody id="content_data">    
                            </tbody>
                        </table>
                        <div class="col-md-12 text-center my-4">
                            <i class="fas fa-spinner fa-spin icon-load" id="icon_spinner"></i>
                        </div>
                    </div>
                </main>

                <!------------INICIO FORMULARIO 1------------>  

                <div class="col-lg-6" id="card-añadir-producto">
                    <div class="card shadow">
                        <div class="card-header">
                            Registrar producto
                            <button class="close" id="close">
                                <span class="fas fa-arrow-right"></span>
                            </button>
                        </div>
                        <div class="card-body">
                            <form class="row container mx-auto" id="form_producto" method="post" enctype="multipart/form-data">
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Nombre producto:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-box"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title="el nombre de la prenda" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{3,48}" placeholder="Nombre" name="nombre" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Tipo:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-box"></i>
                                            </span>
                                        </div>
                                        <select class="form-control" name="tipo" required id="content_data_types"></select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <label class="text-paragraft col-form-label">Descripción:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-paragraph"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" title="la descripción de la prenda" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{3,48}" placeholder="Descripcion corta" name="descripcion" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Referencia:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-barcode"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" placeholder="Referencia" name="referencia" id="input_referencia">                                   
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Cantidad:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-barcode"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" placeholder="Cantidad" name="cantidad" id="input_cantidad">
                                        <div class="input-group-append">
                                            <!-- <button class="btn btn-outline-secondary" type="button" id="button_addon">Generar</button> -->
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Talla:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-box"></i>
                                            </span>
                                        </div>
                                        <select class="form-control" name="talla" required id="content_data_tallas"></select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="text-paragraft col-form-label">Precio:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-dollar-sign"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-gray" placeholder="Precio" name="precio" required>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <label class="text-paragraft col-form-label">Imagen:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupFileAddon01"><i class="fas fa-image"></i></span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input input-gray" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="file">
                                            <label class="custom-file-label" for="inputGroupFile01">Examinar</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 mt-4">             
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="catalogo" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Incluir en el catálogo</label>
                                    </div>
                                </div>

                                <div class="col-lg-12 mt-4">
                                    <button class="btn btn-mybtn btn-block" type="submit">Registrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
    
                <!------------FIN FORMULARIO 1------------>  


                <!------------INICIO FORMULARIO 5------------>  
                <div class="col-lg-6" id="card-añadir-producto-tipo">
                    <div class="card shadow">
                        <div class="card-header">
                            Registrar tipo de prenda
                            <button class="close" id="close">
                                <span class="fas fa-arrow-right"></span>
                            </button>
                        </div>
                        <div class="card-body">
                            <form class="row container mx-auto" id="form_tipos">
                            <label class="text-paragraft col-form-label">Tipo de prenda:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-box"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control input-gray" title="el tipo de prenda" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{3,48}" placeholder="Tipo de prenda" name="nombreTipo" required>    
                                </div>
                                <div class="col-12">
                                    <hr>
                                </div>
                                <div class="col-lg-12 mt-4">
                                    <button class="btn btn-mybtn btn-block" type="submit">Registrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!------------FIN FORMULARIO 5------------>  

            </div>
            <i class="fas fa-arrow-up icon-up"></i>
        </div>
        
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/dashboad_page/croppie.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/toastr.js"></script>
        <script src="../../assets/js/jquery-confirm.js"></script>
        <script src="../../assets/js/dashboad_page/inventory.js"></script>

    </body>
</html>