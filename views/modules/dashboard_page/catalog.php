<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Catálogo</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Comfortaa|Poiret+One|Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../assets/css/dashboard_page/catalog.css">
        <link rel="stylesheet" href="../../assets/css/toastr.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    </head>
    <body>
    
        <div class="container-fluid">
            <div class="row">
                <?php 
                    include "./navbar.php";
                ?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    <div class="d-flex justify-content-between my-3">
                        <h2>Catálogo</h2>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm animated fadeIn" id="table">
                            <thead>
                                <tr>
                                    <th><span>Nombre</span> </th>
                                    <th><span>Descripción</span> </th>
                                    <th><span>Referencia</span> </th>
                                    <th><span>Precio</span> </th>
                                    <th><span>Estado</span> </th>
                                    <th><span>Imagen</span> </th>
                                    <th><span>Acciones</span></th>
                                </tr>
                            </thead>
                            <tbody id="content_data">
                            </tbody>
                        </table>
                        <div class="col-md-12 text-center my-4">
                            <i class="fas fa-spinner fa-spin icon-load" id="icon_spinner"></i>
                        </div>
                    </div>
                </main>
                <i class="fas fa-arrow-up icon-up"></i>
            </div>
        </div>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>
<script src="../../assets/js/jquery.min.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/js/dashboad_page/catalog.js"></script>
<script src="../../assets/js/toastr.js"></script>
<script src="../../assets/js/jquery-confirm.js"></script>

</body>
</html>