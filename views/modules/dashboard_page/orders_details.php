<?php
  include('../../../model/conn.model.php');
  include('../../../model/details_orders/read.details_orders.php');
  session_start();
  if (!isset($_SESSION["user"])){
      header ("Location: ../../../index.php");
  }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Detalle del pedido</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Comfortaa|Poiret+One|Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../assets/css/dashboard_page/orders.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <?php 
                    include "./navbar.php";
                ?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    <div class="d-flex align-items-center my-3">
                        <a href="./orders.php">
                            <i class="fas fa-arrow-left text-muted" style="font-size:1.2rem;"></i>
                        </a>
                        <h2 class="ml-4">Detalle del pedido</h2>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm animated fadeIn">
                            <thead>
                                <tr>
                                    <th>Imagen</th>
                                    <th>Producto</th>
                                    <th>Talla</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody id="content_data"> 
                                <?php foreach ($result as $data): ?>
                                    <tr>
                                        <td><img src="../../assets/images/shop_page/clothes/<?php echo $data['imagenRuta'] ?>" alt="" width="70"></td>
                                        <td> <?php echo $data['nombre'] ?></td>
                                        <td> <?php echo $data['talla_nombre'] ?></td>
                                        <td> <?php echo $data['cantidad'] ?></td>                            
                                    </tr>
                                <?php endforeach?>
                            </tbody>
                        </table>
                    </div>
                </main>
                <i class="fas fa-arrow-up icon-up"></i>
            </div>
        </div>
        <!-- Icons -->
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/toastr.js"></script>
        <script src="../../assets/js/jquery-confirm.js"></script>
    </body>

</html>