<nav class="navbar navbar-dark fixed-top flex-md-nowrap p-0 shadow-sm ">
    <a class="navbar-brand col-lg-2 mr-0" href="index_admin.php">
    <img src="../../assets/images/landing_page/logo_blanco.png" alt="" width="80" id="image_logo">
    </a>
    <div class="container px-4">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" type="text" placeholder="Buscar" aria-label="Search" id="searchInput">
        </div>
    </div>
</nav>
<nav class=" col-lg-2 main-menu ">
    <ul>
        <a href="index_admin.php">
            <li>
                <i class="fa fa-home fa-2x"></i>
                <span class="nav-text">
                    Inicio
                </span>
            </li>
        </a>
        <a href="employees.php">
            <li class="">
                <i class="fa fa-users fa-2x"></i>
                <span class="nav-text">
                    Empleados
                </span>
            </li>
        </a>
        <a href="providers.php">
            <li class="">
                <i class="fa fa-dolly fa-2x"></i>
                <span class="nav-text">
                    Proveedores
                </span>
            </li>
        </a>
        <a href="inventory.php">
            <li class="">
                <i class="fa fa-box-open fa-2x"></i>
                <span class="nav-text">
                    Inventario
                </span>
            </li>
        </a>
        <a href="supplies.php">
            <li class="">
                <i class="fa fa-th-list fa-2x"></i>
                <span class="nav-text">
                    Insumos
                </span>
            </li>
        </a>
        <a href="catalog.php">
            <li class="">
                <i class="fa fa-tshirt fa-2x"></i>
                <span class="nav-text">
                    Catálogo
                </span>
            </li>
        </a>
        <a href="client.php">
            <li class="">
                <i class="fa fa-user-tag fa-2x"></i>
                <span class="nav-text">
                    Clientes
                </span>
            </li>
        </a>

        <a href="orders.php">
            <li class="">
                <i class="fa fa-tasks fa-2x"></i>
                <span class="nav-text">
                    Pedidos
                </span>
            </li>
        </a>
        <a href="usuarios.php">
            <li class="">
                <i class="fa fa-user fa-2x"></i>
                <span class="nav-text">
                    Usuarios
                </span>
            </li>
        </a>
        <ul class="section-abajo">
            <a href="profile.php">
                <li class="">
                    <i class="fa fa-user-circle fa-2x"></i>
                    <span class="nav-text">
                        Perfil
                    </span>
                </li>
            </a>
            <a href="./cerrar.php" id="salir">
                <li class="">
                    <i class="fa fa-power-off fa-2x"></i>
                    <span class="nav-text">
                        Salir
                    </span>
                </li>
            </a>
        </ul>
    </ul>
</nav>