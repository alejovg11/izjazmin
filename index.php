<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Landing page</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <link rel="stylesheet" href="views/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="views/assets/css/landing_page/index.css">
    <link rel="stylesheet" href="views/assets/css/toastr.css">
    <link rel="stylesheet" href="views/assets/css/hover-min.css">

</head>
<body>
    <!-- login -->
    <div class="modal fade" id="exampleModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Iniciar sesión</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close2">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form_login">
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <label class="text-paragraft col-form-label">Correo electrónico:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control input-gray" title="tu correo" placeholder="Correo" name="correo" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <label class="text-paragraft col-form-label">Contraseña:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-lock"></i>
                                    </span>
                                </div>
                                <input type="password" class="form-control input-gray" title="tu contraseña" placeholder="Contraseña" name="contrasena" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-mybtn" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-mybtn">Iniciar sesión</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- signup -->
    <div class="modal fade" id="exampleModal2">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Registro</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="row" id="form_signup">
                        <div class="col-lg-6">
                            <label class="text-paragraft col-form-label">Nombre:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-user"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control input-gray" title="tu nombre" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" placeholder="Nombre" name="nombre" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label class="text-paragraft col-form-label">Apellido:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-user"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control input-gray" title="tu apellido" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,64}" placeholder="Apellido" name="apellido" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label class="text-paragraft col-form-label">Identificación:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-id-card"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control input-gray" title="tu documento" pattern="[0-9]{8,10}" maxlength="10"  placeholder="Identificacion" name="identificacion" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label class="text-paragraft col-form-label">Teléfono:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-phone"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control input-gray" title ="tu telefono" pattern="[0-9]{7}" maxlength="7" placeholder="Telefono" name="telefono" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <label class="text-paragraft col-form-label">Dirección:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-home"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control input-gray" title="tu dirección" placeholder="Direccion" name="direccion" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <label class="text-paragraft col-form-label">Correo electrónico:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                </div>
                                <input type="email" class="form-control input-gray" title="tu correo" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Correo" name="correo2" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <label class="text-paragraft col-form-label">Contraseña:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-lock"></i>
                                    </span>
                                </div>
                                    <input type="password" class="form-control input-gray" title="tu contraseña" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*)(?=.*[a-z]).*$" placeholder="Contraseña" name="contrasena2" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <p class="text-muted text-info-validate">Utiliza ocho caracteres como mínimo con una combinación de letras, números o simbolos</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-mybtn" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-mybtn">Registrarme</button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light fixed-top">
        <a class="navbar-brand" href="#">
        <img src="views/assets/images/landing_page/logo.png" alt="" width="80" id="image_logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ">
                <li class="nav-item active">
                    <a class="nav-link" id="login" data-toggle="modal" data-target="#exampleModal" >Iniciar sesión</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal2">Registrarme</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="section_1">
        <div class="container">
            <div class="row">
                <!-- titulo -->
                <div class="col-sm-12 col-lg-6">
                    <img src="views/assets/images/landing_page/logo.png" class="logo" alt="">
                    <p class="my-4 parrafo_inicio">isJazmin está para servirte como tienda virtual.</p>
                    <a href="views/modules/shop_page" role="button" class="btn btn-outline-mybtn hvr-icon-wobble-horizontal">Ir a la tienda
                        <i class="fas fa-arrow-right hvr-icon"></i>
                    </a>

                </div>
            </div>
        </div>
    </div>
    <div class="section_2">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card text-center hvr-curl-top-right">
                        <div class="card-body">
                            <div class="d-flex justify-content-center">
                                <div class="circle">
                                    <img src="views/assets/images/landing_page/shopping-list.svg" alt="" width="30">
                                </div>
                            </div>
                                <h5 class="mt-3">NECESIDAD</h5>
                            <div>
                                <p class="card-text text-muted">Llevar el control de gestión fácil, con isJazmin podrás llevar el control de todos los procesos necesarios fácilmente. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card text-center hvr-curl-top-right">
                        <div class="card-body">
                            <div class="d-flex justify-content-center">
                                <div class="circle">
                                    <img src="views/assets/images/landing_page/icon.svg" alt="" width="30">
                                </div>
                            </div>
                            <h5 class="mt-3">APORTE</h5>
                            <p class="card-text text-muted">Permiten a la empresa acceder a múltiples beneficios entre ellos es agilizar los procesos de registros de datos.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card text-center hvr-curl-top-right">
                        <div class="card-body">
                            <div class="d-flex justify-content-center">
                                <div class="circle">
                                    <img src="views/assets/images/landing_page/ok.svg" alt="" width="30">
                                </div>
                            </div>
                            <h5 class="mt-3">FACILIDAD</h5>
                            <p class="card-text text-muted">Para crear cotizaciones de ventas, registros tanto como de personal, como de todo lo de producción, así mismo garantizando una seguridad en los datos.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay"></div>
    <script src="views/assets/js/jquery.min.js"></script>
    <script src="views/assets/js/bootstrap.min.js"></script>
    <script src="views/assets/js/landing_page/index.js"></script>
    <script src="views/assets/js/toastr.js"></script>

</body>

</html>