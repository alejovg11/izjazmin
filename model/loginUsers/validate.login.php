<?php 
session_start();

include('../conn.model.php');
$correo = $_POST['correo'];
$contrasena = $_POST['contrasena'];
try {
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT clientes.id, clientes.nombre, clientes.apellido, clientes.telefono, clientes.direccion, clientes.ident, usuarios.correo, usuarios.contrasena, usuarios.rol_id from clientes INNER JOIN usuarios on clientes.usuarios_id = usuarios.id WHERE correo = ?";
    $query = $pdo->prepare($sql);
    $query->execute(array($correo));
    $result = $query->fetch(PDO::FETCH_ASSOC);
    if ($result["contrasena"]==$contrasena) {
        $return = array(true,"/isJazmin/views/modules/shop_page/",$result);
        $_SESSION["client"] = $result;
    } else {
        $return = array(false,"correo o contraseña inválidos");
    }
    echo json_encode($return);
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }

?>
