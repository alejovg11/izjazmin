<?php

  include('../conn.model.php');

  try {
    $nombre = $_POST['nombre']; 
    $apellido = $_POST['apellido']; 
    $correo = $_POST['correo']; 
    $ident = $_POST['ident']; 
    $telefono = $_POST['telefono']; 
    $direccion = $_POST['direccion']; 
    $id = $_POST['id'];
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "UPDATE clientes SET nombre = ?, apellido = ?, ident = ?, telefono = ?, direccion = ? WHERE id = ?";
    $query = $pdo->prepare($sql);
    $query->execute(array($nombre,$apellido,$ident,$telefono,$direccion,$id));
    echo "profile Update Successfully";
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }


?>
