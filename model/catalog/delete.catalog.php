<?php

include('../conn.model.php');

  if (isset($_POST['id'])) {
    try {
      $id = $_POST['id'];
      $pdo = DataBase::connect();
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sql = "DELETE FROM catalogo WHERE id = ?";
      $query = $pdo->prepare($sql);
      $query->execute(array($id));
      echo "Catalog Deleted Successfully";
    } catch (PDOException $e) {
      die($e->getMessage()."".$e->getLine()."".$e->getFile());
    }
  }

?>
