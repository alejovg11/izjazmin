<?php
  include('../conn.model.php');
  $search = $_POST['search'];   
if(!empty($search)){ 
  try {
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // $sql = "SELECT * FROM catalogo WHERE nombre LIKE '%$search%'";
    $sql = "SELECT catalogo.id, catalogo.estado, productos.nombre, productos.referencia, productos.imagenRuta, productos.descripcion, productos.precio from catalogo INNER JOIN productos on catalogo.producto_id = productos.id WHERE productos.referencia LIKE '%$search%'";    
    $query = $pdo->prepare($sql);
    $query->execute();
    $result = $query->fetchALL(PDO::FETCH_ASSOC);
    echo json_encode($result);
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }
}else{
  try {
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT catalogo.id, catalogo.estado, productos.nombre, productos.referencia, productos.imagenRuta, productos.descripcion, productos.precio from catalogo INNER JOIN productos on catalogo.producto_id = productos.id";
    $query = $pdo->prepare($sql);
    $query->execute();
    $result = $query->fetchALL(PDO::FETCH_ASSOC);
    echo json_encode($result);
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }
}
?>
