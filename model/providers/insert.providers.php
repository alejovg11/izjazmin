<?php

  include('../conn.model.php');

  $nombre = $_POST['nombre'];
  $nombre_ger = $_POST['nombre_ger'];
  $correo = $_POST['correo'];
  $correo_ger = $_POST['correo_ger'];
  $telefono = $_POST['telefono'];
  $telefono_ger = $_POST['telefono_ger'];
  $direccion = $_POST['direccion'];
  $nit = $_POST['nit'];

  if(validate($correo_ger, $correo, $nit) == 1){
    echo 2;
  } else {
    try {
      $pdo = DataBase::connect();
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sqlOrders = "INSERT INTO proveedores(nombre, nombre_ger, correo, correo_ger, telefono, telefono_ger, direccion, nit) VALUES (?,?,?,?,?,?,?,?)";
      $query = $pdo->prepare($sqlOrders);
      $query->execute(array($nombre,$nombre_ger,$correo,$correo_ger,$telefono,$telefono_ger,$direccion,$nit));
      echo "Provider Added Successfully";  
    } catch (PDOException $e) {
      die($e->getMessage()."".$e->getLine()."".$e->getFile());
    }
  }


  
  function validate($correo_ger, $correo, $nit){
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql="SELECT * from proveedores 
      where correo = ? OR correo_ger = ? OR nit = ?";
      $query = $pdo->prepare($sql);
      $query->execute(array($correo_ger, $correo, $nit));
      $row_count = $query->fetchColumn();
      if($row_count > 0){
        return 1;
      }else{
        return 0;
      }
  }


  
?>
