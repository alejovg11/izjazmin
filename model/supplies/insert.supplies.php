<?php

  include('../conn.model.php');

if(isset($_POST['nombreInsumo'])) {
  $nombreInsumo = $_POST['nombreInsumo'];
  $cantidad = $_POST['cantidad2'];
  $unidad = $_POST['unidad'];       
  try {
      $pdo = DataBase::connect();
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sqlSupplies = "INSERT INTO insumos(nombre, cantidad, unidad_id) VALUES (?,?,?)";
      $query = $pdo->prepare($sqlSupplies);
      $query->execute(array($nombreInsumo,$cantidad,$unidad));
      echo "Supplies Added Successfully";  
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }

}
  
?>
