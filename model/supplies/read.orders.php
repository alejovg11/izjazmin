<?php

  include('../conn.model.php');
  try {
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT insumos_proveedores.id, insumos_proveedores.cantidad, insumos_proveedores.precio, insumos.nombre, unidades.nombre_unidad, proveedores.nombre as nombre_proveedor FROM (((insumos_proveedores INNER JOIN insumos ON insumos_proveedores.insumo_id = insumos.id) INNER JOIN unidades ON insumos_proveedores.unidad_id = unidades.id) INNER JOIN proveedores ON insumos_proveedores.proveedor_id = proveedores.id)";
    $query = $pdo->prepare($sql);
    $query->execute();
    $result = $query->fetchALL(PDO::FETCH_ASSOC);
    echo json_encode($result);
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }
?>
