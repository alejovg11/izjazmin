<?php

  include('../conn.model.php');

if(isset($_POST['insumo'])) {
  $insumo = $_POST['insumo'];
  $cantidad = $_POST['cantidad'];
  $unidades = $_POST['unidades'];
  $empresa = $_POST['empresa'];
  $precio = $_POST['precio'];
  try {
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sqlOrders = "INSERT INTO insumos_proveedores(proveedor_id, insumo_id, precio, unidad_id, cantidad) VALUES (?,?,?,?,?)";
    $query = $pdo->prepare($sqlOrders);
    $query->execute(array($empresa,$insumo,$precio,$unidades,$cantidad));
    echo "Orders Added Successfully";  
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }  
}
  
?>
