<?php

  include('../conn.model.php');
  
if(isset($_POST['nombre'])) {
    $nombre  = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];
    $tipo = $_POST['tipo'];
    $referencia = $_POST['referencia'];
    $precio = $_POST['precio'];
    $talla = $_POST['talla'];
    $cantidad = $_POST['cantidad'];
    $catalogo = $_POST['catalogo'];

   $flag=false;
   $tmp = $_FILES["file"]["tmp_name"];
   $ruta = "../../views/assets/images/shop_page/clothes/";
   $Ext  = pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
   if (!$Ext == "jpg") {
     $return =  array(false,"Sube un archivo");
   }else{
     if ($tmp!="") {
       $flag=true;
       $remplazar = array(" ","Ñ","ñ","*",",");
       $_FILES["file"]["name"] = str_replace($remplazar,'-',$_FILES["file"]["name"]);
       $archivo = $ruta.date('d-s').$_FILES["file"]["name"];
     }else{
       $flag=false;
     }
     if ($flag==true) {
       if (move_uploaded_file($tmp,$archivo)) {
        $img=date('d-s').$_FILES["file"]["name"];
        $return = array(true,"Guardo con exito","galeria");
       }
   }
  
    try {
      $pdo = DataBase::connect();
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $producto = "INSERT INTO productos(tipo_id, nombre, descripcion, referencia, precio, imagenRuta) VALUES (?, ?, ?, ?, ?, ?)";
      $query = $pdo->prepare($producto);
      $query->execute(array($tipo, $nombre, $descripcion, $referencia, $precio, $img));
      $id = $pdo->lastInsertId();

      $producto_detalle = "INSERT INTO producto_detalle(producto_id, talla_id) VALUES (?, ?)";
      $query = $pdo->prepare($producto_detalle);
      $query->execute(array($id,$talla)); 
      $idDetalle = $pdo->lastInsertId();

      $inventario = "INSERT INTO inventario(prod_det_id, cantidad) VALUES (?, ?)";
      $query = $pdo->prepare($inventario);
      $query->execute(array($idDetalle, $cantidad)); 
     
      if ($catalogo == "on") {
        $sqlCatalog = "INSERT INTO catalogo(producto_id, detalle_id, estado) VALUES (?,?,?)";
        $query = $pdo->prepare($sqlCatalog);
        $query->execute(array($id, $idDetalle, "disponible"));
      }

    } catch (PDOException $e) {
      die($e->getMessage()."".$e->getLine()."".$e->getFile());
    } 
  }
}
?>
