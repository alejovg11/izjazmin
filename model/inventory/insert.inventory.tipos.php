<?php

  include('../conn.model.php');
  
  $nombre = $_POST['nombreTipo'];     
  if(validate($nombre) == 1){
    echo 2;
  } else {
  try {
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $producto = "INSERT INTO tipos(nombre) VALUES (?)";
    $query = $pdo->prepare($producto);
    $query->execute(array($nombre));
    echo "Tipo Added Successfully";  
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }
}

function validate($nombre){
  $pdo = DataBase::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql="SELECT * from tipos 
    where nombre = ?";
    $query = $pdo->prepare($sql);
    $query->execute(array($nombre));
    $row_count = $query->fetchColumn();
    if($row_count > 0){
      return 1;
    }else{
      return 0;
    }
}


  
?>
