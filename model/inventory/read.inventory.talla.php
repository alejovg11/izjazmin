
<?php

include('../conn.model.php');
try {
  $pdo = DataBase::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "SELECT * FROM tallas";
  $query = $pdo->prepare($sql);
  $query->execute();
  $result = $query->fetchALL(PDO::FETCH_ASSOC);
  echo json_encode($result);
} catch (PDOException $e) {
  die($e->getMessage()."".$e->getLine()."".$e->getFile());
}
?>
