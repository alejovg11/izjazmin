<?php

  include('../conn.model.php');
  try {
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT * FROM productos INNER JOIN producto_detalle ON productos.id = producto_detalle.producto_id INNER JOIN inventario ON inventario.prod_det_id = producto_detalle.id";
    $query = $pdo->prepare($sql);
    $query->execute();
    $result = $query->fetchALL(PDO::FETCH_ASSOC);
    echo json_encode($result);
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }
?>
