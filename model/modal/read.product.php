<?php

  include('../conn.model.php');
  
  $id = $_POST['id'];
  try {
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT catalogo.id, catalogo.estado, productos.nombre, productos.referencia, productos.descripcion, productos.precio from catalogo INNER JOIN productos on catalogo.producto_id = productos.id WHERE catalogo.id = ?";
    $query = $pdo->prepare($sql);
    $query->execute(array($id));
    $result = $query->fetchALL(PDO::FETCH_ASSOC);
    echo json_encode($result);
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }

?>
