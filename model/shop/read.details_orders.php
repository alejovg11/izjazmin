<?php
    if ($_GET) {
        $id = $_GET['id'];
        try {
            $pdo = DataBase::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT productos.nombre, productos.precio, productos.imagenRuta, detalle_pedido.cantidad, detalle_pedido.talla_nombre FROM detalle_pedido INNER JOIN productos ON detalle_pedido.producto_id = productos.id WHERE pedido_id = ?";
            $query = $pdo->prepare($sql);
            $query->execute(array($id));
            $result = $query->fetchALL(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            die($e->getMessage()."".$e->getLine()."".$e->getFile());
        }
    }
?>
