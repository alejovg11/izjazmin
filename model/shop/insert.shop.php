<?php
 require_once('../conn.model.php');
    $json = file_get_contents('php://input');
    $data = json_decode($json);
    $Arraylength = count($data);
    $ArrayProduct=[];

    for ($i=0; $i <$Arraylength - 1 ; $i++) {
      array_push($ArrayProduct, $data[$i]);      
    }

    try {
      $pdo = DataBase::connect();
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $fecha = date("Y-m-d");
      $sqlPedidos = "INSERT INTO pedidos(cliente_id, estado, fecha) VALUES (?,?,?)";   
      $query = $pdo->prepare($sqlPedidos);
      $query->execute(array($data[$Arraylength-1]->id, 'pendiente', $fecha));
      $id = $pdo->lastInsertId();

      foreach ($ArrayProduct as $row) {
        $sqlDetallePedido = "INSERT INTO detalle_pedido(pedido_id, producto_id, talla_nombre, cantidad) VALUES (?,?,?,?)";   
        $query = $pdo->prepare($sqlDetallePedido);
        $query->execute(array($id, $row->product->id, $row->product->talla, $row->cantidad));
          echo "Added Successfully";  
      } 
    } catch (PDOException $e) {
        die($e->getMessage()."".$e->getLine()."".$e->getFile());
    }
    

?>