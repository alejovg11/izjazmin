<?php

if ($_GET) {
    $id = $_GET['id'];
    try {
        $pdo = DataBase::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM clientes WHERE id = ?";
        $query = $pdo->prepare($sql);
        $query->execute(array($id));
        $result = $query->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die($e->getMessage()."".$e->getLine()."".$e->getFile());
    }
}
    
?>