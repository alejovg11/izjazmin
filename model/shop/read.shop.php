<?php

  include('../conn.model.php');

  try {
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT productos.id, inventario.cantidad as cantidadInventario, catalogo.estado, productos.nombre, productos.referencia, productos.imagenRuta, productos.descripcion, productos.precio, tallas.nombre as talla from ((((catalogo INNER JOIN productos on catalogo.producto_id = productos.id) INNER JOIN producto_detalle ON catalogo.detalle_id = producto_detalle.id) INNER JOIN tallas ON producto_detalle.talla_id = tallas.id) INNER JOIN inventario on producto_detalle.id = inventario.prod_det_id)";
    
    $query = $pdo->prepare($sql);
    $query->execute();
    $result = $query->fetchALL(PDO::FETCH_ASSOC);
    echo json_encode($result);
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }
?>
