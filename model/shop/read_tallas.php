<?php

  include('../conn.model.php');

  try {
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT tallas.nombre AS nombre_talla, productos.nombre FROM ((producto_detalle INNER JOIN productos ON producto_detalle.producto_id = productos.id) INNER JOIN tallas ON producto_detalle.talla_id = tallas.id)";
    $query = $pdo->prepare($sql);
    $query->execute();
    $result = $query->fetchALL(PDO::FETCH_ASSOC);
    echo json_encode($result);
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }
?>
