<?php

  include('../conn.model.php');
  try {
    $pdo = DataBase::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // $sql = "SELECT productos.nombre as producto, clientes.nombre, detalle_pedido.cantidad, pedidos.fecha, productos.referencia, pedidos.estado FROM (((detalle_pedido INNER JOIN pedidos ON detalle_pedido.pedido_id = pedidos.id) INNER JOIN clientes ON pedidos.cliente_id = clientes.id) INNER JOIN productos ON detalle_pedido.producto_id = productos.id)";
    $sql = "SELECT pedidos.id, clientes.nombre, pedidos.estado, pedidos.fecha FROM pedidos INNER JOIN clientes ON pedidos.cliente_id = clientes.id";
    $query = $pdo->prepare($sql);
    $query->execute();
    $result = $query->fetchALL(PDO::FETCH_ASSOC);
    echo json_encode($result);
  } catch (PDOException $e) {
    die($e->getMessage()."".$e->getLine()."".$e->getFile());
  }
?>
