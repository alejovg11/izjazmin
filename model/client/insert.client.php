<?php

 include('../conn.model.php');

 $nombre = $_POST['nombre'];
 $apellido = $_POST['apellido'];
 $identificacion = $_POST['identificacion'];
 $telefono = $_POST['telefono'];
 $direccion = $_POST['direccion'];
 $correo = $_POST['correo2'];
 $contrasena = $_POST['contrasena2'];
 
 if(validate($correo) == 1){
   echo 2;
 } else {
   try {
     $pdo = DataBase::connect();
     $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     $sqlUsuarios = "INSERT INTO usuarios(correo, contrasena, rol_id) VALUES (?, ?, '3')";
     $queryUser = $pdo->prepare($sqlUsuarios);
     $queryUser->execute(array($correo,$contrasena));
     $id = $pdo->lastInsertId();
 
     $sqlClientes = "INSERT INTO clientes(nombre, apellido, ident, telefono, direccion, usuarios_id) VALUES (?, ?, ?, ?, ?, ?)";
     $query = $pdo->prepare($sqlClientes);
     $query->execute(array($nombre,$apellido,$identificacion,$telefono,$direccion,$id)); 
     echo "Client Added Successfully";  
   } catch (PDOException $e) {
     die($e->getMessage()."".$e->getLine()."".$e->getFile());
   }
 }
 
 function validate($correo){
   $pdo = DataBase::connect();
   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $sql="SELECT * from usuarios 
     where correo = ?";
     $query = $pdo->prepare($sql);
     $query->execute(array($correo));
     $row_count = $query->fetchColumn();
     if($row_count > 0){
       return 1;
     }else{
       return 0;
     }
 }
  
?>
